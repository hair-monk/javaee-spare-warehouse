import Vue from 'vue'

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import router from './router'
import App from './App.vue'
import '@/styles/index.scss'

Vue.use(ElementUI);
Vue.config.productionTip = false
let user = localStorage.getItem("user");
let path = window.location.hash;
if (!user && path != "#/login") {
  window.location.href = window.location.origin + "#/login";
}

router.beforeEach((to, from, next) => {
  if (to.path === '/login') {
    sessionStorage.removeItem('user');
  }
  var user = localStorage.getItem('user');
  if (!user && to.path !== '/login') {
    next({
      path: '/login'
    })
  } else {
    next();
  }
})

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
