import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/page/login.vue'
import container from '@/components/container.vue';
import student from "@/components/student.vue";
import course from "@/components/course.vue";
import score from "@/components/score.vue";
import xuanke from "@/components/xuanke.vue";
import kaoqin from "@/components/kaoqin.vue";
import daka from "@/components/daka.vue";

import index from "@/components/index.vue";
Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'container',
            component: container,
            children: [
                {
                    path: "/",
                    name: "index",
                    component: index,
                },
                {
                    path: "/student",
                    name: "student",
                    component: student,
                },
                {
                    path: "/course",
                    name: "course",
                    component: course,
                },
                {
                    path: "/score",
                    name: "score",
                    component: score,
                },
                {
                    path: "/xuanke",
                    name: "xuanke",
                    component: xuanke,
                },
                {
                    path: "/daka",
                    name: "daka",
                    component: daka,
                },
                {
                    path: "/kaoqin",
                    name: "kaoqin",
                    component: kaoqin,
                },
            ]

        },
        {
            path: '/login',
            name: 'Login',
            component: Login
        },

    ]
})