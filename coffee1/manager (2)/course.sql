/*
Navicat MySQL Data Transfer

Source Server         : wjcy
Source Server Version : 50527
Source Host           : localhost:3306
Source Database       : course

Target Server Type    : MYSQL
Target Server Version : 50527
File Encoding         : 65001

Date: 2020-06-14 17:16:00
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `userName` varchar(255) DEFAULT NULL COMMENT '用户名',
  `password` int(11) DEFAULT NULL COMMENT '密码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='管理员表';

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES ('1', 'admin', '111111');

-- ----------------------------
-- Table structure for course
-- ----------------------------
DROP TABLE IF EXISTS `course`;
CREATE TABLE `course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `courseNo` varchar(255) DEFAULT NULL COMMENT '课程号',
  `name` varchar(50) DEFAULT NULL COMMENT '课程名称',
  `year` varchar(50) DEFAULT NULL COMMENT '学期',
  `period` varchar(50) DEFAULT NULL COMMENT '学时',
  `score` double(11,2) NOT NULL COMMENT '学分',
  `teacherId` int(11) DEFAULT NULL COMMENT '授课教师',
  `delFlag` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of course
-- ----------------------------
INSERT INTO `course` VALUES ('1', '1592053582392', '计算机原理', '2020上学期', '32课时', '10.00', '1', '0');
INSERT INTO `course` VALUES ('2', '1592102735541', '经济', '2020下学期', '15', '5.00', '4', '1');
INSERT INTO `course` VALUES ('3', '1592102766886', '英语', '2020下学期', '15', '8.00', '4', '0');

-- ----------------------------
-- Table structure for kaoqin
-- ----------------------------
DROP TABLE IF EXISTS `kaoqin`;
CREATE TABLE `kaoqin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `courseId` int(11) DEFAULT NULL COMMENT '选课编号',
  `createTime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of kaoqin
-- ----------------------------
INSERT INTO `kaoqin` VALUES ('1', '1', '2020-06-13 23:15:21');
INSERT INTO `kaoqin` VALUES ('2', '1', '2020-06-14 00:00:26');
INSERT INTO `kaoqin` VALUES ('3', '1', '2020-06-14 00:09:34');
INSERT INTO `kaoqin` VALUES ('4', '2', '2020-06-14 10:47:40');

-- ----------------------------
-- Table structure for score
-- ----------------------------
DROP TABLE IF EXISTS `score`;
CREATE TABLE `score` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `studentCourseId` int(11) DEFAULT NULL COMMENT '学生选课记录id',
  `score` double(255,2) DEFAULT NULL COMMENT '得分',
  `createTime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='成绩表';

-- ----------------------------
-- Records of score
-- ----------------------------
INSERT INTO `score` VALUES ('8', '1', '48.00', '2020-06-13 22:15:15');
INSERT INTO `score` VALUES ('9', '1', '58.00', '2020-06-14 00:02:46');
INSERT INTO `score` VALUES ('10', '1', '60.00', '2020-06-14 00:10:36');
INSERT INTO `score` VALUES ('11', '2', '58.00', '2020-06-14 10:48:55');

-- ----------------------------
-- Table structure for student
-- ----------------------------
DROP TABLE IF EXISTS `student`;
CREATE TABLE `student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `xueHao` varchar(50) DEFAULT NULL COMMENT '学号',
  `name` varchar(50) DEFAULT NULL COMMENT '姓名',
  `subject` varchar(50) DEFAULT NULL COMMENT '专业',
  `sex` varchar(50) DEFAULT NULL COMMENT '1 男 2女',
  `delFlag` int(11) DEFAULT NULL COMMENT '0 未删除 1 已删除',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='学生表';

-- ----------------------------
-- Records of student
-- ----------------------------
INSERT INTO `student` VALUES ('1', '1591974042571', '张三', '计算机', '男', '0', '111111');
INSERT INTO `student` VALUES ('2', '1591974103296', '里斯', '化工', '男', '0', '111111');
INSERT INTO `student` VALUES ('3', '1592102703930', '哈哈', '经管系', '女', '1', '111111');

-- ----------------------------
-- Table structure for student_course
-- ----------------------------
DROP TABLE IF EXISTS `student_course`;
CREATE TABLE `student_course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `courseId` int(255) DEFAULT NULL COMMENT '课程',
  `studentId` int(50) DEFAULT NULL COMMENT '课程名称',
  `delFlag` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT '学时',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='学生的选课';

-- ----------------------------
-- Records of student_course
-- ----------------------------
INSERT INTO `student_course` VALUES ('1', '1', '1', '0', '1');
INSERT INTO `student_course` VALUES ('2', '3', '1', '0', '0');

-- ----------------------------
-- Table structure for teacher
-- ----------------------------
DROP TABLE IF EXISTS `teacher`;
CREATE TABLE `teacher` (
  `id` int(11) NOT NULL,
  `userName` varchar(255)ccourseourse DEFAULT NULL COMMENT '用户名',
  `realName` varchar(50) DEFAULT NULL COMMENT '姓名',
  `password` int(11) DEFAULT NULL COMMENT '密码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='教师表';

-- ----------------------------
-- Records of teacher
-- ----------------------------
INSERT INTO `teacher` VALUES ('1', 'tec001', '张老师', '111111');
INSERT INTO `teacher` VALUES ('2', 'tec002', '王老师', '111111');
INSERT INTO `teacher` VALUES ('3', 'tec003', '刘老师', '111111');
INSERT INTO `teacher` VALUES ('4', 'tec004', '陈老师', '111111');
coursecoursecourse