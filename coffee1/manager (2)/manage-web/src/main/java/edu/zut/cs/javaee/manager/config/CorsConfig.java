package edu.zut.cs.javaee.manager.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

/**
 * 处理跨域 扫描config包下的CORSConfig类，用来解决axios的跨域请求问题
 	 不建这个类的话在前端部分使用axios（一种ajax的封装）请求数据是会失败！
 *
 */
@Configuration
public class CorsConfig  {
    @Bean
    public CorsFilter corsFilter(){
        CorsConfiguration cors = new CorsConfiguration();
        //不填具体的url cookie 将不能使用
        cors.addAllowedOrigin("*");
        //是否携带cookie 信息 false不携带
        cors.setAllowCredentials(false);
        //允许的方法
        cors.addAllowedMethod(HttpMethod.DELETE);
        cors.addAllowedMethod(HttpMethod.GET);
        cors.addAllowedMethod(HttpMethod.HEAD);
        cors.addAllowedMethod(HttpMethod.POST);
        cors.addAllowedMethod(HttpMethod.PUT);
        cors.addAllowedMethod(HttpMethod.PATCH);
        cors.addAllowedMethod(HttpMethod.OPTIONS);
        cors.addAllowedMethod(HttpMethod.TRACE);
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**",cors);
        return new CorsFilter(source);
    }
}
