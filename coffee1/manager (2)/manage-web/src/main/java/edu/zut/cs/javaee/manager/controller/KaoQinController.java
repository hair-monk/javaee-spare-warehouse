package edu.zut.cs.javaee.manager.controller;

import edu.zut.cs.javaee.manager.base.util.Page;
import edu.zut.cs.javaee.manager.base.util.PageList;
import edu.zut.cs.javaee.manager.kaoqin.KaoQinService;
import edu.zut.cs.javaee.manager.kaoqin.domain.KaoQin;
import edu.zut.cs.javaee.manager.score.ScoreService;
import edu.zut.cs.javaee.manager.score.domain.Score;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
/**
 * 
 * @author Zhao Haobin
 *
 */
@RestController
@RequestMapping("/kaoqin")
public class KaoQinController {
    @Autowired
    private KaoQinService kaoQinService;
    /**
     * 获取考勤列表
     * @param name  课程名称
     * @param userId 当前操作者id
     * @param role   角色 1 管理员 2教师 3 学生
     * @param pageNo 当前页
     * @param pageSize 每页大小
     * @return
     * @throws Exception
     */
    @PostMapping(value = "/getKaoQinList")
    public ResponseEntity<PageList<KaoQin>> getKaoQinList(String name, Integer userId, Integer role, Integer pageNo, Integer pageSize) throws Exception {
        Page page = new Page();
        page.setCurrentPage(pageNo);
        page.setEveryPage(pageSize);
        PageList<KaoQin> list = kaoQinService.getKaoQinList(page,name,userId,role);
        if (!ObjectUtils.isEmpty(list.getList())){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            for (KaoQin kaoQin:list.getList()) {
                    kaoQin.setTime(sdf.format(kaoQin.getCreateTime()));
            }
        }
        return ResponseEntity.ok(list);

    }

}
