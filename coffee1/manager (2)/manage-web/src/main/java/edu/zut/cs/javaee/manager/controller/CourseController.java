package edu.zut.cs.javaee.manager.controller;

import edu.zut.cs.javaee.manager.base.util.Page;
import edu.zut.cs.javaee.manager.base.util.PageList;
import edu.zut.cs.javaee.manager.course.CourseService;
import edu.zut.cs.javaee.manager.course.domain.Course;
import edu.zut.cs.javaee.manager.studentcourse.domain.StudentCourse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletContext;
import java.math.BigDecimal;

/**
 * 课程管理
 */
@RestController
@RequestMapping("/course")
public class CourseController {
    @Autowired
    private CourseService courseService;

    @Autowired
    private ServletContext context;


    /**
     * 获取课程信息
     * @param name  课程名称
     * @param userId 当前操作者id
     * @param role   角色 1 管理员 2教师 3 学生
     * @param pageNo 当前页
     * @param pageSize 每页大小
     * @return
     * @throws Exception
     */
    @PostMapping(value = "/getCourseList")
    public ResponseEntity<PageList<Course>> getCourseList(String name,Integer userId,Integer role,Integer pageNo,Integer pageSize) throws Exception {
        Page page = new Page();
        page.setCurrentPage(pageNo);
        page.setEveryPage(pageSize);
        PageList<Course> list = courseService.getCourseList(page,name,userId,role);
        return ResponseEntity.ok(list);

    }

    /**
     * 新增课程
     * @param name 课程名称
     * @param year 开课学期
     * @param period 学时
     * @param score 学分
     * @param teacherId 授课教师
     * @return
     * @throws Exception 抛出异常
     */
    @PostMapping(value = "/add")
    public ResponseEntity<Integer> add(String name, String year, String period, BigDecimal score,Integer teacherId) throws Exception {
        Course course = new Course();
        course.setDelFlag(0);
        course.setScore(score);
        course.setPeriod(period);
        course.setYear(year);
        course.setName(name);
        course.setCourseNo(System.currentTimeMillis() + "");
        course.getTeacher().setId(teacherId);
        courseService.add(course);
        return ResponseEntity.ok(200);

    }

    /**
     * 课程更新
     * @param name 课程名称
     * @param year 开课学期
     * @param period 学时
     * @param score 学分
     * @param teacherId 授课教师
     * @return
     * @throws Exception
     */
    @PostMapping(value = "/update")
    public ResponseEntity<Integer> update(Integer id ,String name, String year, String period, BigDecimal score,Integer teacherId) throws Exception {
        Course course = new Course();
        course.setId(id);
        course.setDelFlag(0);
        course.setScore(score);
        course.setPeriod(period);
        course.setYear(year);
        course.setName(name);
        course.getTeacher().setId(teacherId);
        courseService.update(course);
        return ResponseEntity.ok(200);

    }

    /**
     * 删除课程信息
     * @param id
     * @return
     * @throws Exception
     */
    @PostMapping(value = "/delete")
    public ResponseEntity<Integer> delete(Integer id) throws Exception {
        Course course = new Course();
        course.setId(id);
        courseService.delete(course);
        return ResponseEntity.ok(200);

    }

    /**
     * 删除课程信息
     * @param id
     * @return
     * @throws Exception
     */
    @PostMapping(value = "/xuanKe")
    public ResponseEntity<Integer> xuanKe(Integer id,Integer userId) throws Exception {
        StudentCourse sc = new StudentCourse();
        sc.getCourse().setId(id);
        sc.getStudent().setId(userId);
        sc.setDelFlag(0);
        sc.setStatus(0);
        courseService.xuanKe(sc);
        return ResponseEntity.ok(200);

    }

}
