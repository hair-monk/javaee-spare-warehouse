package edu.zut.cs.javaee.manager.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * 启动类
 */
@EnableTransactionManagement
@SpringBootApplication
@ComponentScan("edu.zut")
@EntityScan(basePackages = ("edu.zut.*"))
public class Application {
	//@SpringBootApplication：包含了@ComponentScan、@Configuration和@EnableAutoConfiguration注解。其中@ComponentScan让spring Boot扫描到Configuration类并把它加入到程序上下文。
	//@EnableTransactionManagement 开启事务支持
	//@ComponentScan 组件扫描，可自动发现和装配一些Bean。
	public static void main(String[] args) {
        SpringApplication.run(Application.class,args);
    }
}
