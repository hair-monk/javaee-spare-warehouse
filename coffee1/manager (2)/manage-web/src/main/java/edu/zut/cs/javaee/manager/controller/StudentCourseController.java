package edu.zut.cs.javaee.manager.controller;

import edu.zut.cs.javaee.manager.base.util.Page;
import edu.zut.cs.javaee.manager.base.util.PageList;
import edu.zut.cs.javaee.manager.course.CourseService;
import edu.zut.cs.javaee.manager.course.domain.Course;
import edu.zut.cs.javaee.manager.studentcourse.StudentCourseService;
import edu.zut.cs.javaee.manager.studentcourse.domain.StudentCourse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletContext;
import java.math.BigDecimal;

/**
 * 学生选课管理
 */
@RestController
@RequestMapping("/stcourse")
public class StudentCourseController {
    @Autowired
    private StudentCourseService studentCourseService;

    /**
     * 获取信息列表
     * @param name  课程名称
     * @param userId 当前操作者id
     * @param role   角色 1 管理员 2教师 3 学生
     * @param pageNo 当前页
     * @param pageSize 每页大小
     * @param status 课程状态
     * @return
     * @throws Exception
     */
    @PostMapping(value = "/getStCourseList")
    public ResponseEntity<PageList<StudentCourse>> getStCourseList(String name,Integer userId,Integer role,Integer pageNo,Integer pageSize,Integer status) throws Exception {
        Page page = new Page();
        page.setCurrentPage(pageNo);
        page.setEveryPage(pageSize);
        PageList<StudentCourse> list = studentCourseService.getStCourseList(page,name,userId,role,status);
        return ResponseEntity.ok(list);

    }

    /**
     * 课程打分
     * @param id 选课记录id
     * @param score 学分
     * @return
     * @throws Exception
     */
    @PostMapping(value = "/daFen")
    public ResponseEntity<Integer> daFen(BigDecimal score,Integer id) throws Exception {
        studentCourseService.daFen(score,id);
        return ResponseEntity.ok(200);

    }

    /**
     * 退选课程
     * @param id
     * @return
     * @throws Exception
     */
    @PostMapping(value = "/tuiXuan")
    public ResponseEntity<Integer> tuiXuan(Integer id) throws Exception {

        studentCourseService.tuiXuan(id);
        return ResponseEntity.ok(200);

    }

    /**
     * 重选课程
     * @param id
     * @return
     * @throws Exception
     */
    @PostMapping(value = "/chongXiu")
    public ResponseEntity<Integer> chongXiu(Integer id) throws Exception {

        studentCourseService.chongXiu(id);
        return ResponseEntity.ok(200);

    }

    /**
     * 课程打卡
     * @param id
     * @return
     * @throws Exception
     */
    @PostMapping(value = "/daKa")
    public ResponseEntity<Integer> daKa(Integer id) throws Exception {

        studentCourseService.daKa(id);
        return ResponseEntity.ok(200);

    }
}
