package edu.zut.cs.javaee.manager.controller;

import edu.zut.cs.javaee.manager.teacher.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import java.util.Map;

/**
 * 教师controller
 */
@RestController
@RequestMapping("/teacher")
public class TeacherController {
    @Autowired
    private TeacherService teacherService;

    @PostMapping(value = "/getTeacherNameAndId")
    public ResponseEntity<List<Map<String,Object>>> getTeacherNameAndId(){
        List<Map<String,Object>> list = teacherService.getTeacherNameAndId();
        return ResponseEntity.ok(list);
    }
}