package edu.zut.cs.javaee.manager.controller;

import edu.zut.cs.javaee.manager.base.util.Page;
import edu.zut.cs.javaee.manager.base.util.PageList;
import edu.zut.cs.javaee.manager.score.ScoreService;
import edu.zut.cs.javaee.manager.score.domain.Score;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

/**
 * 学生成绩管理
 */
@RestController
@RequestMapping("/score")
public class ScoreController {
    @Autowired
    private ScoreService scoreService;

    /**
     * 获取成绩列表
     * @param name  couresname
     * @param userId 当前操作者id
     * @param role    1 admin	2teacher 3student
     * @param pageNo 当前页
     * @param pageSize   Display number per page
     * @return
     * @throws Exception
     *  ResponseEntity.ok --creating a ResponseEntity with the given body andthe status set to OK.
     */
    @PostMapping(value = "/getScoreList")
    public ResponseEntity<PageList<Score>> getScoreList(String name, Integer userId, Integer role, Integer pageNo, Integer pageSize) throws Exception {
        Page page = new Page();
        page.setCurrentPage(pageNo);
        page.setEveryPage(pageSize);
        PageList<Score> list = scoreService.getScoreList(page,name,userId,role);
        return ResponseEntity.ok(list);

    }

    /**
     * 课程打分 editScore
     * @param id 成绩id
     * @param score 学分
     * @return json
     * @throws Exception
     * 使用ResponseEntity设置 HTTP响应的正文，状态和标头。
     *  @PostMapping post请求
     */
    @PostMapping(value = "/editScore")
    public ResponseEntity<Integer> editScore(BigDecimal score,Integer id) throws Exception {
        scoreService.editScore(score,id);
        return ResponseEntity.ok(200);

    }

}
