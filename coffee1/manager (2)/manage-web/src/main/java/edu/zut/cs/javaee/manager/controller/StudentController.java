package edu.zut.cs.javaee.manager.controller;

import edu.zut.cs.javaee.manager.admin.AdminService;
import edu.zut.cs.javaee.manager.base.exception.GlobalException;
import edu.zut.cs.javaee.manager.base.util.Page;
import edu.zut.cs.javaee.manager.base.util.PageList;
import edu.zut.cs.javaee.manager.student.StudentService;
import edu.zut.cs.javaee.manager.student.domain.Student;
import edu.zut.cs.javaee.manager.teacher.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletContext;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
//lwn
@RestController
@RequestMapping("/student")
public class StudentController {
    @Autowired
    private StudentService studentService;

    @Autowired
    private ServletContext context;


    /**
     * 获取学生信息
     * @param name  姓名
     * @param userId 当前操作者id
     * @param role   角色 1 管理员 2教师 3 学生
     * @param pageNo 当前页
     * @param pageSize 每页大小
     * @return
     * @throws Exception
     */
    @PostMapping(value = "/getStudentList")
    public ResponseEntity<PageList<Student>> getStudentList(String name,Integer userId,Integer role,Integer pageNo,Integer pageSize) throws Exception {
        Page page = new Page();
        page.setCurrentPage(pageNo);
        page.setEveryPage(pageSize);
        PageList<Student> list = studentService.getStudentList(page,name,userId,role);
        return ResponseEntity.ok(list);

    }

    /**
     * 新增学生
     * @param name 姓名
     * @param sex 性别
     * @param subject 专业
     * @return
     * @throws Exception
     */
    @PostMapping(value = "/add")
    public ResponseEntity<Integer> add(String name,String sex,String subject) throws Exception {
        Student s = new Student();
        s.setDelFlag(0);
        s.setName(name);
        s.setPassword("111111");
        s.setSex(sex);
        s.setSubject(subject);
        s.setXueHao(System.currentTimeMillis() + "");
        studentService.add(s);
        return ResponseEntity.ok(200);

    }

    /**
     * 更新学生信息
     * @param id
     * @param name
     * @param sex
     * @param subject
     * @return 
     * @throws Exception
     */
    @PostMapping(value = "/update")
    public ResponseEntity<Integer> update(Integer id,String name,String sex,String subject) throws Exception {
        Student s = new Student();
        s.setId(id);
        s.setName(name);
        s.setSex(sex);
        s.setSubject(subject);
        studentService.update(s);
        return ResponseEntity.ok(200);

    }

    /**
     * 删除学生信息
     * @param id
     * @return
     * @throws Exception
     */
    @PostMapping(value = "/delete")
    public ResponseEntity<Integer> delete(Integer id) throws Exception {
        Student s = new Student();
        s.setId(id);
        studentService.delete(s);
        return ResponseEntity.ok(200);

    }
}
