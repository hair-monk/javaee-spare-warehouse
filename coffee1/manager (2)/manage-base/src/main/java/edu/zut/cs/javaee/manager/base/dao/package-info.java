/**
 * Data Access Object (dao) Base Classes
 * 
 * @author Zhao Haobin
 *
 */
package edu.zut.cs.javaee.manager.base.dao;