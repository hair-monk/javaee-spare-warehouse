package edu.zut.cs.javaee.manager.base.dao;

import java.io.Serializable;

import edu.zut.cs.javaee.manager.base.domain.BaseTreeEntity;

/**
 * 
 * @author liuxiaoming <a href="mailto:ming616@gmail.com">Liu Xiaoming</a>
 *
 * @param <PK>the primary key for that type 
 * @param <T>a type variable of domain entity
 * @param <M> domain manager
 */
public class GenericTreeDaoTestCase<PK extends Serializable, T extends BaseTreeEntity<T, PK>, M extends GenericTreeDao<T, PK>>
		extends GenericDaoTestCase<PK, T, M> {

	public GenericTreeDaoTestCase(Class<T> persistentClass) {
		super(persistentClass);
	}

}
