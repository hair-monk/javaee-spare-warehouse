package edu.zut.cs.javaee.manager.base.exception;

/**
 * 全局异常
 */
public class GlobalException extends  Exception {

    private Integer status;

    public GlobalException(){}

    public GlobalException(int status,String msg) {
        super(msg);
        this.status = status;
    }
    public GlobalException(String msg) {
        super(msg);
        this.status = 400;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
