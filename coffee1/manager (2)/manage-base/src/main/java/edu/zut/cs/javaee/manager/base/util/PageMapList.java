package edu.zut.cs.javaee.manager.base.util;

import java.util.List;
import java.util.Map;
/**
 * 
 * @author Is.f
 *
 */
public class PageMapList {
	private Page page;  
	  
    private List<Map<String,Object>> list;  
  
    /** 
     * The default constructor 
     */  
    public PageMapList(){  
        super();
    }  
  
    /** 
     * The constructor using fields 
     *  
     * @param page
     */  
    public PageMapList(Page page, List<Map<String,Object>> list) {  
        this.page = page;  
        this.list = list;  
    }

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public List<Map<String,Object>> getList() {
		return list;
	}

	public void setList(List<Map<String,Object>> list) {
		this.list = list;
	}  

}
