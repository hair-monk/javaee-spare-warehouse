package edu.zut.cs.javaee.manager.studentcourse.service;

import edu.zut.cs.javaee.manager.base.service.GenericManager;
import edu.zut.cs.javaee.manager.student.domain.Student;
import edu.zut.cs.javaee.manager.studentcourse.domain.StudentCourse;

public interface StudentCourseManager extends GenericManager<StudentCourse, Long> {
}
