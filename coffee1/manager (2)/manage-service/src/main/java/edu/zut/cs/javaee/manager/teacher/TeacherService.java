package edu.zut.cs.javaee.manager.teacher;

import edu.zut.cs.javaee.manager.teacher.dao.TeacherDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @author fy
 * 教师service
 */
@Service
@Transactional
public class TeacherService {
    @Autowired
    private TeacherDao teacherDao;
    public List getUserInfo(String userName, String passWord) {
        return teacherDao.findList("from Teacher where userName = ?0 and password = ?1",userName,passWord);
    }

    public List<Map<String, Object>> getTeacherNameAndId() {
        return teacherDao.findListBySqlNoEntity("select id,realName from teacher");
    }
}
