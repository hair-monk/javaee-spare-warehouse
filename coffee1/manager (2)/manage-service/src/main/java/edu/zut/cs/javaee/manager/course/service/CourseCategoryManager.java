package edu.zut.cs.javaee.manager.course.service;

import edu.zut.cs.javaee.manager.base.service.GenericTreeManager;
import edu.zut.cs.javaee.manager.course.domain.CourseCategory;
import edu.zut.cs.javaee.manager.student.domain.StudentCategory;

public interface CourseCategoryManager extends GenericTreeManager<CourseCategory, Long> {
}
