package edu.zut.cs.javaee.manager.admin.service.impl;

import edu.zut.cs.javaee.manager.admin.dao.AdminDao;
import edu.zut.cs.javaee.manager.admin.domain.Admin;
import edu.zut.cs.javaee.manager.admin.service.AdminManager;
import edu.zut.cs.javaee.manager.base.service.impl.GenericManagerImpl;
import edu.zut.cs.javaee.manager.course.domain.Course;
import edu.zut.cs.javaee.manager.course.service.CourseManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service(value = "AdminManager")
public class AdminManagerImpl extends GenericManagerImpl<Admin, Long> implements AdminManager {

    AdminDao adminDao;

    @Autowired
    public void setAdminDao(AdminDao adminDao){
        this.adminDao=adminDao;
        this.dao=this.adminDao;
    }
}
