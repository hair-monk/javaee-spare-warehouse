package edu.zut.cs.javaee.manager.kaoqin.service;

import edu.zut.cs.javaee.manager.base.service.GenericTreeManager;
import edu.zut.cs.javaee.manager.course.domain.CourseCategory;
import edu.zut.cs.javaee.manager.kaoqin.domain.KaoQin;
import edu.zut.cs.javaee.manager.kaoqin.domain.KaoQinCategory;

public interface KaoQinCategoryManager  extends GenericTreeManager<KaoQinCategory, Long> {
}
