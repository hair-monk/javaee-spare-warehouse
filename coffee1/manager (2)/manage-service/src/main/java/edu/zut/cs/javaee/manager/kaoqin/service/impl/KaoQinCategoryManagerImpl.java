package edu.zut.cs.javaee.manager.kaoqin.service.impl;

import edu.zut.cs.javaee.manager.base.service.impl.GenericTreeManagerImpl;
import edu.zut.cs.javaee.manager.course.domain.CourseCategory;
import edu.zut.cs.javaee.manager.course.service.CourseCategoryManager;
import edu.zut.cs.javaee.manager.kaoqin.dao.KaoQinCategoryDao;
import edu.zut.cs.javaee.manager.kaoqin.domain.KaoQin;
import edu.zut.cs.javaee.manager.kaoqin.domain.KaoQinCategory;
import edu.zut.cs.javaee.manager.kaoqin.service.KaoQinCategoryManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service(value = "KaoQinCategoryManager")
public class KaoQinCategoryManagerImpl extends GenericTreeManagerImpl<KaoQinCategory, Long>
        implements KaoQinCategoryManager {


//    ProductCategoryDao productCategoryDao;
//
//    @Autowired
//    public void setProductCategoryDao(ProductCategoryDao productCategoryDao) {
//        this.productCategoryDao = productCategoryDao;
//        this.dao = this.productCategoryDao;
//    }
    KaoQinCategoryDao kaoQinCategoryDao;

    @Autowired
    public void setKaoQinCategoryDao(KaoQinCategoryDao kaoQinCategoryDao){
        this.kaoQinCategoryDao=kaoQinCategoryDao;
        this.dao=this.kaoQinCategoryDao;
    }

}
