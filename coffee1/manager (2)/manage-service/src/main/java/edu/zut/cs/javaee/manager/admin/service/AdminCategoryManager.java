package edu.zut.cs.javaee.manager.admin.service;

import edu.zut.cs.javaee.manager.admin.domain.Admin;
import edu.zut.cs.javaee.manager.admin.domain.AdminCategory;
import edu.zut.cs.javaee.manager.base.service.GenericTreeManager;
import edu.zut.cs.javaee.manager.course.domain.CourseCategory;

public interface AdminCategoryManager extends GenericTreeManager<AdminCategory, Long> {
}
