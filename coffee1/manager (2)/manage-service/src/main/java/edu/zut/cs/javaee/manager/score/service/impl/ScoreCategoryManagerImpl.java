package edu.zut.cs.javaee.manager.score.service.impl;

import edu.zut.cs.javaee.manager.base.service.impl.GenericTreeManagerImpl;
import edu.zut.cs.javaee.manager.course.domain.CourseCategory;
import edu.zut.cs.javaee.manager.course.service.CourseCategoryManager;
import edu.zut.cs.javaee.manager.score.dao.ScoreCategoryDao;
import edu.zut.cs.javaee.manager.score.domain.Score;
import edu.zut.cs.javaee.manager.score.domain.ScoreCategory;
import edu.zut.cs.javaee.manager.score.service.ScoreCategoryManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service(value = "ScoreCategoryManager")
public class ScoreCategoryManagerImpl extends GenericTreeManagerImpl<ScoreCategory, Long>
        implements ScoreCategoryManager {

    ScoreCategoryDao scoreCategoryDao;

    @Autowired
    public void setScoreCategoryDao(ScoreCategoryDao scoreCategoryDao){
        this.scoreCategoryDao=scoreCategoryDao;
        this.dao=this.scoreCategoryDao;
    }
}
