package edu.zut.cs.javaee.manager.admin;

//管理员
import edu.zut.cs.javaee.manager.admin.dao.AdminDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
/**
 * @author Is.f-hzya
 *Get user information
 */
@Service
public class AdminService {

    @Autowired
    private AdminDao adminDao;

    @Transactional
    public List getUserInfo(String userName, String passWord) {
    	//访问数据库
    	//调用base方法，实现带参数的查询
    	return adminDao.findList("from Admin where userName = ?0 and password = ?1",userName,passWord); 
    }
    
}
