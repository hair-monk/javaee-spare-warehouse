package edu.zut.cs.javaee.manager.tuition.service;

import edu.zut.cs.javaee.manager.base.service.GenericTreeManager;
import edu.zut.cs.javaee.manager.student.domain.StudentCategory;
import edu.zut.cs.javaee.manager.tuition.domain.Tuition;
import edu.zut.cs.javaee.manager.tuition.domain.TuitionCategory;

public interface TuitionCategoryManager  extends GenericTreeManager<TuitionCategory, Long> {
}
