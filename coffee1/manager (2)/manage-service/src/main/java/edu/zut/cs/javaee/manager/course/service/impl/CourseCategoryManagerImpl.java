package edu.zut.cs.javaee.manager.course.service.impl;


import edu.zut.cs.javaee.manager.base.service.impl.GenericTreeManagerImpl;
import edu.zut.cs.javaee.manager.course.dao.CourseCategoryDao;
import edu.zut.cs.javaee.manager.course.domain.Course;
import edu.zut.cs.javaee.manager.course.domain.CourseCategory;
import edu.zut.cs.javaee.manager.course.service.CourseCategoryManager;
import edu.zut.cs.javaee.manager.student.domain.StudentCategory;
import edu.zut.cs.javaee.manager.student.service.StudentCategoryManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service(value = "CourseCategoryManager")
public class CourseCategoryManagerImpl extends GenericTreeManagerImpl<CourseCategory, Long>
        implements CourseCategoryManager {


//    ProductCategoryDao productCategoryDao;
//
//    @Autowired
//    public void setProductCategoryDao(ProductCategoryDao productCategoryDao) {
//        this.productCategoryDao = productCategoryDao;
//        this.dao = this.productCategoryDao;
//    }

    CourseCategoryDao courseCategoryDao;

    @Autowired
    public void setCourseCategoryDao(CourseCategoryDao courseCategoryDao)
    {
        this.courseCategoryDao = courseCategoryDao;
        this.dao = this.courseCategoryDao;
    }

}
