package edu.zut.cs.javaee.manager.tuition.service.impl;

import edu.zut.cs.javaee.manager.base.service.impl.GenericManagerImpl;
import edu.zut.cs.javaee.manager.student.domain.Student;
import edu.zut.cs.javaee.manager.student.service.StudentManager;
import edu.zut.cs.javaee.manager.tuition.dao.TuitionDao;
import edu.zut.cs.javaee.manager.tuition.domain.Tuition;
import edu.zut.cs.javaee.manager.tuition.service.TuitionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service(value = "TuitionManager")
public class TuitionManagerImpl extends GenericManagerImpl<Tuition, Long> implements TuitionManager {

    //    ProductDao productDao;
//
//    @Autowired
//    public void setProductDao(ProductDao productDao) {
//        this.productDao = productDao;
//        this.dao = this.productDao;
//    }
    TuitionDao tuitionDao;

    @Autowired
    public void setTuitionDao(TuitionDao tuitionDao){
        this.tuitionDao=tuitionDao;
        this.dao=this.tuitionDao;
    }
}
