package edu.zut.cs.javaee.manager.admin.service;

import edu.zut.cs.javaee.manager.admin.domain.Admin;
import edu.zut.cs.javaee.manager.base.service.GenericManager;
import edu.zut.cs.javaee.manager.course.domain.Course;

public interface AdminManager extends GenericManager<Admin, Long> {
}
