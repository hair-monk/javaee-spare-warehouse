package edu.zut.cs.javaee.manager.teacher.service;

import edu.zut.cs.javaee.manager.base.service.GenericManager;
import edu.zut.cs.javaee.manager.studentcourse.domain.StudentCourse;
import edu.zut.cs.javaee.manager.teacher.domain.Teacher;

public interface TeacherManager extends GenericManager<Teacher, Long> {
}
