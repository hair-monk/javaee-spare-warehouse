package edu.zut.cs.javaee.manager.tuition.service;

import edu.zut.cs.javaee.manager.base.service.GenericManager;
import edu.zut.cs.javaee.manager.student.domain.Student;
import edu.zut.cs.javaee.manager.tuition.domain.Tuition;

public interface TuitionManager extends GenericManager<Tuition, Long> {
}
