package edu.zut.cs.javaee.manager.kaoqin;

import edu.zut.cs.javaee.manager.base.util.Page;
import edu.zut.cs.javaee.manager.base.util.PageList;
import edu.zut.cs.javaee.manager.kaoqin.dao.KaoQinDao;
import edu.zut.cs.javaee.manager.kaoqin.domain.KaoQin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

/**
 * 
 * @author Zhao Haobin
 *
 */
@Service
@Transactional
public class KaoQinService {

    @Autowired
    private KaoQinDao kaoQinDao;

    public PageList<KaoQin> getKaoQinList(Page page, String name, Integer userId, Integer role) {

        String hql = "from KaoQin where 1=1 ";

        if (!ObjectUtils.isEmpty(name)) {
            hql += " and course.course.name like '%" + name + "%'";
        }

        if (role == 2) {
            //教师
            hql += " and course.course.teacher.id = " + userId;
        }

        if (role == 3) {
            //学生
            hql += " and course.student.id = " + userId;
        }

        return kaoQinDao.findPageListByHql(page,hql);

    }
}
