package edu.zut.cs.javaee.manager.studentcourse.service.impl;


import edu.zut.cs.javaee.manager.base.service.impl.GenericTreeManagerImpl;
import edu.zut.cs.javaee.manager.student.domain.StudentCategory;
import edu.zut.cs.javaee.manager.student.service.StudentCategoryManager;
import edu.zut.cs.javaee.manager.studentcourse.dao.StudentCourseTypeDao;
import edu.zut.cs.javaee.manager.studentcourse.domain.StudentCourseType;
import edu.zut.cs.javaee.manager.studentcourse.service.StudentCourseTypeManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service(value = "StudentCourseCategoryManager")
public class StudentCourseTypeManagerImpl  extends GenericTreeManagerImpl<StudentCourseType, Long>
        implements StudentCourseTypeManager {

    StudentCourseTypeDao studentCourseTypeDao;


    @Autowired
    public void setStudentCourseTypeDao(StudentCourseTypeDao studentCourseTypeDao)
    {
        this.studentCourseTypeDao = studentCourseTypeDao;
        this.dao = this.studentCourseTypeDao;
    }


}
