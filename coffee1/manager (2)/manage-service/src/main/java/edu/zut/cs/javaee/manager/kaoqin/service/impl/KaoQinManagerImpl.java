package edu.zut.cs.javaee.manager.kaoqin.service.impl;

import edu.zut.cs.javaee.manager.base.service.impl.GenericManagerImpl;
import edu.zut.cs.javaee.manager.course.domain.Course;
import edu.zut.cs.javaee.manager.course.service.CourseManager;
import edu.zut.cs.javaee.manager.kaoqin.dao.KaoQinDao;
import edu.zut.cs.javaee.manager.kaoqin.domain.KaoQin;
import edu.zut.cs.javaee.manager.kaoqin.service.KaoQinManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service(value = "KaoQinManager")
public class KaoQinManagerImpl extends GenericManagerImpl<KaoQin, Long> implements KaoQinManager {

    //    ProductDao productDao;
//
//    @Autowired
//    public void setProductDao(ProductDao productDao) {
//        this.productDao = productDao;
//        this.dao = this.productDao;
//    }
    KaoQinDao kaoQinDao;

    @Autowired
    public void setKaoQinDao(KaoQinDao kaoQinDao){
        this.kaoQinDao=kaoQinDao;
        this.dao=this.kaoQinDao;
    }
}
