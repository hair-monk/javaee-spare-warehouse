package edu.zut.cs.javaee.manager.tuition.service.impl;

import edu.zut.cs.javaee.manager.base.service.impl.GenericTreeManagerImpl;
import edu.zut.cs.javaee.manager.student.domain.StudentCategory;
import edu.zut.cs.javaee.manager.student.service.StudentCategoryManager;
import edu.zut.cs.javaee.manager.tuition.dao.TuitionCategoryDao;
import edu.zut.cs.javaee.manager.tuition.domain.Tuition;
import edu.zut.cs.javaee.manager.tuition.domain.TuitionCategory;
import edu.zut.cs.javaee.manager.tuition.service.TuitionCategoryManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service(value = "TuitionCategoryManager")
public class TuitionCategoryManagerImpl extends GenericTreeManagerImpl<TuitionCategory, Long>
        implements TuitionCategoryManager {

    //    ProductCategoryDao productCategoryDao;
//
//    @Autowired
//    public void setProductCategoryDao(ProductCategoryDao productCategoryDao) {
//        this.productCategoryDao = productCategoryDao;
//        this.dao = this.productCategoryDao;
//    }
    TuitionCategoryDao tuitionCategoryDao;

    @Autowired
    public void setTuitionCategoryDao(TuitionCategoryDao tuitionCategoryDao){
        this.tuitionCategoryDao=tuitionCategoryDao;
        this.dao=this.tuitionCategoryDao;
    }
}
