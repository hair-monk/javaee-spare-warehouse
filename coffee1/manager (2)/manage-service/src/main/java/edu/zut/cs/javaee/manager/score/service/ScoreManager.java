package edu.zut.cs.javaee.manager.score.service;

import edu.zut.cs.javaee.manager.base.service.GenericManager;
import edu.zut.cs.javaee.manager.course.domain.Course;
import edu.zut.cs.javaee.manager.score.domain.Score;

public interface ScoreManager extends GenericManager<Score, Long> {
}
