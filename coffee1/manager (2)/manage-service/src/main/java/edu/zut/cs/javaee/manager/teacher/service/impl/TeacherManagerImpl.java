package edu.zut.cs.javaee.manager.teacher.service.impl;

import edu.zut.cs.javaee.manager.base.service.impl.GenericManagerImpl;
import edu.zut.cs.javaee.manager.student.domain.Student;
import edu.zut.cs.javaee.manager.student.service.StudentManager;
import edu.zut.cs.javaee.manager.teacher.dao.TeacherDao;
import edu.zut.cs.javaee.manager.teacher.domain.Teacher;
import edu.zut.cs.javaee.manager.teacher.service.TeacherManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service(value = "TeacherManager")
public class TeacherManagerImpl extends GenericManagerImpl<Teacher, Long> implements TeacherManager {

    TeacherDao teacherDao;

    @Autowired
    public void setTeacherDao(TeacherDao teacherDao){
        this.teacherDao=teacherDao;
        this.dao=this.teacherDao;
    }
}
