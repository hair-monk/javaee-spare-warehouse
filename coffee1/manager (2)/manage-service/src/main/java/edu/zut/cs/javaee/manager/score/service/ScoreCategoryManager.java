package edu.zut.cs.javaee.manager.score.service;

import edu.zut.cs.javaee.manager.base.service.GenericTreeManager;
import edu.zut.cs.javaee.manager.course.domain.CourseCategory;
import edu.zut.cs.javaee.manager.score.domain.Score;
import edu.zut.cs.javaee.manager.score.domain.ScoreCategory;

public interface ScoreCategoryManager extends GenericTreeManager<ScoreCategory, Long> {
}
