package edu.zut.cs.javaee.manager.studentcourse;

import edu.zut.cs.javaee.manager.base.exception.GlobalException;
import edu.zut.cs.javaee.manager.base.util.Page;
import edu.zut.cs.javaee.manager.base.util.PageList;
import edu.zut.cs.javaee.manager.kaoqin.dao.KaoQinDao;
import edu.zut.cs.javaee.manager.kaoqin.domain.KaoQin;
import edu.zut.cs.javaee.manager.score.dao.ScoreDao;
import edu.zut.cs.javaee.manager.score.domain.Score;
import edu.zut.cs.javaee.manager.studentcourse.dao.StudentCourseDao;
import edu.zut.cs.javaee.manager.studentcourse.domain.StudentCourse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.util.Date;

@Service
@Transactional
public class StudentCourseService {

    @Autowired
    private StudentCourseDao studentCourseDao;

    @Autowired
    private ScoreDao scoreDao;

    @Autowired
    private KaoQinDao kaoQinDao;


    public PageList<StudentCourse> getStCourseList(Page page, String name, Integer userId, Integer role,Integer status) {
        String hql = "from StudentCourse where delFlag = 0";
        if (!ObjectUtils.isEmpty(name)) {
            hql += " and course.name like '%" + name + "%'";
        }

        if (role == 2) {
            //教师
            hql += " and course.teacher.id = " + userId;
        }

        if (role == 3) {
            //学生
            hql += " and student.id = " + userId;
        }

        if (!ObjectUtils.isEmpty(status)) {
            hql += " and status = " + status;
        }

        return studentCourseDao.findPageListByHql(page,hql);
    }

    public void daFen(BigDecimal score, Integer id) throws Exception {
        StudentCourse course = studentCourseDao.get(id);
        if (course.getStatus() == 1) {
            throw new GlobalException("当前课程已通过考核，要修改成绩请前往成绩管理");
        }
        if(course.getStatus() == 3) {
            throw new GlobalException("当前课程已经退选，无法打分");
        }
        if (score.intValue() >= 60) {
            course.setStatus(1);
        } else {
            course.setStatus(2);
        }
        Score scores = new Score();
        scores.setCreateTime(new Date());
        scores.setScore(score);
        scores.getStudentCourse().setId(course.getId());
        scoreDao.save(scores);
    }

    public void tuiXuan(Integer id) throws Exception {
        StudentCourse course = studentCourseDao.get(id);
        if (course.getStatus() == 0) {
            course.setStatus(3);
        } else {
            throw new GlobalException("只有处于未考核状态的课程可以退选");
        }

    }

    public void chongXiu(Integer id) throws Exception {
        StudentCourse course = studentCourseDao.get(id);
        if (course.getStatus() == 3 || course.getStatus() == 2) {
            course.setStatus(0);
        } else {
            throw new GlobalException("只有处于退选状态和考核未通过的课程可以重修");
        }
    }

    public void daKa(Integer id) {
        StudentCourse course = studentCourseDao.get(id);
        KaoQin kaoQin = new KaoQin();
        kaoQin.setCourse(course);
        kaoQin.setCreateTime(new Date());
        kaoQin.setCreateTime(new Date());
        kaoQinDao.save(kaoQin);
    }
}
