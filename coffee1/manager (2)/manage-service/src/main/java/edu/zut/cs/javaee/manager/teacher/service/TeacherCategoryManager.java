package edu.zut.cs.javaee.manager.teacher.service;

import edu.zut.cs.javaee.manager.base.service.GenericTreeManager;
import edu.zut.cs.javaee.manager.studentcourse.domain.StudentCourseType;
import edu.zut.cs.javaee.manager.teacher.domain.TeacherCategory;

public interface TeacherCategoryManager extends GenericTreeManager<TeacherCategory, Long> {
}
