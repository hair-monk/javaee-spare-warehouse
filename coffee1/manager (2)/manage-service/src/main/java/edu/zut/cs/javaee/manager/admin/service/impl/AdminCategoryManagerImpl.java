package edu.zut.cs.javaee.manager.admin.service.impl;

import edu.zut.cs.javaee.manager.admin.dao.AdminCategoryDao;
import edu.zut.cs.javaee.manager.admin.domain.Admin;
import edu.zut.cs.javaee.manager.admin.domain.AdminCategory;
import edu.zut.cs.javaee.manager.admin.service.AdminCategoryManager;
import edu.zut.cs.javaee.manager.base.service.impl.GenericTreeManagerImpl;
import edu.zut.cs.javaee.manager.course.domain.CourseCategory;
import edu.zut.cs.javaee.manager.course.service.CourseCategoryManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service(value = "AdminCategoryManager")
public class AdminCategoryManagerImpl extends GenericTreeManagerImpl<AdminCategory, Long>
        implements AdminCategoryManager {
    AdminCategoryDao adminCategoryDao;

    @Autowired
    public void setAdminCategoryDao(AdminCategoryDao adminCategoryDao){
        this.adminCategoryDao=adminCategoryDao;
        this.dao=this.adminCategoryDao;
    }

}
