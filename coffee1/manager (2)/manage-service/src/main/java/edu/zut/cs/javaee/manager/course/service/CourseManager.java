package edu.zut.cs.javaee.manager.course.service;

import edu.zut.cs.javaee.manager.base.service.GenericManager;
import edu.zut.cs.javaee.manager.course.domain.Course;


public interface CourseManager extends GenericManager<Course, Long> {
}
