package edu.zut.cs.javaee.manager.student.service;

import edu.zut.cs.javaee.manager.base.service.GenericManager;
import edu.zut.cs.javaee.manager.student.domain.Student;

public interface StudentManager extends GenericManager<Student, Long> {
}
