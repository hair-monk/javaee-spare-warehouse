package edu.zut.cs.javaee.manager.student.service;

import edu.zut.cs.javaee.manager.base.service.GenericTreeManager;
import edu.zut.cs.javaee.manager.student.domain.StudentCategory;

public interface StudentCategoryManager extends GenericTreeManager<StudentCategory, Long> {
}
