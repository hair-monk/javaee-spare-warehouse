package edu.zut.cs.javaee.manager.student.service.impl;

import com.sun.org.apache.bcel.internal.generic.PUSH;
import edu.zut.cs.javaee.manager.base.service.impl.GenericTreeManagerImpl;
import edu.zut.cs.javaee.manager.student.dao.StudentCategoryDao;
import edu.zut.cs.javaee.manager.student.dao.StudentDao;
import edu.zut.cs.javaee.manager.student.domain.Student;
import edu.zut.cs.javaee.manager.student.domain.StudentCategory;
import edu.zut.cs.javaee.manager.student.service.StudentCategoryManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service(value = "StudentCategoryManager")
public class StudentCategoryManagerImpl extends GenericTreeManagerImpl<StudentCategory, Long>
        implements StudentCategoryManager {

//    ProductCategoryDao productCategoryDao;
//
//    @Autowired
//    public void setProductCategoryDao(ProductCategoryDao productCategoryDao) {
//        this.productCategoryDao = productCategoryDao;
//        this.dao = this.productCategoryDao;
//    }

    StudentCategoryDao studentCategoryDao;
    @Autowired
    public void setStudentCategoryDao(StudentCategoryDao studentCategoryDao)
    {
        this.studentCategoryDao=studentCategoryDao;
        this.dao=this.studentCategoryDao;
    }

}

