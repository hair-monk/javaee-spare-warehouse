package edu.zut.cs.javaee.manager.kaoqin.service;

import edu.zut.cs.javaee.manager.base.service.GenericManager;
import edu.zut.cs.javaee.manager.course.domain.Course;
import edu.zut.cs.javaee.manager.kaoqin.domain.KaoQin;

public interface KaoQinManager extends GenericManager<KaoQin, Long> {
}
