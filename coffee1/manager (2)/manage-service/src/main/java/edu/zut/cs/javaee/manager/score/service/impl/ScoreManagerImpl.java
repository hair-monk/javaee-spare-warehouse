package edu.zut.cs.javaee.manager.score.service.impl;

import edu.zut.cs.javaee.manager.base.service.impl.GenericManagerImpl;
import edu.zut.cs.javaee.manager.course.domain.Course;
import edu.zut.cs.javaee.manager.course.service.CourseManager;
import edu.zut.cs.javaee.manager.score.dao.ScoreDao;
import edu.zut.cs.javaee.manager.score.domain.Score;
import edu.zut.cs.javaee.manager.score.service.ScoreManager;
import net.bytebuddy.asm.Advice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service(value = "ScoreManager")
public class ScoreManagerImpl extends GenericManagerImpl<Score, Long> implements ScoreManager {

    ScoreDao scoreDao;

    @Autowired
    public void setScoreDao(ScoreDao scoreDao){
        this.scoreDao=scoreDao;
        this.dao=this.scoreDao;
    }
}
