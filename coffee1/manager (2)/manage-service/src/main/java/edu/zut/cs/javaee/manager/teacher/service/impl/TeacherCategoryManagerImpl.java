package edu.zut.cs.javaee.manager.teacher.service.impl;

import edu.zut.cs.javaee.manager.base.service.impl.GenericTreeManagerImpl;
import edu.zut.cs.javaee.manager.student.domain.StudentCategory;
import edu.zut.cs.javaee.manager.student.service.StudentCategoryManager;
import edu.zut.cs.javaee.manager.teacher.dao.TeacherCategoryDao;
import edu.zut.cs.javaee.manager.teacher.domain.Teacher;
import edu.zut.cs.javaee.manager.teacher.domain.TeacherCategory;
import edu.zut.cs.javaee.manager.teacher.service.TeacherCategoryManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service(value = "TeacherCategoryManager")
public class TeacherCategoryManagerImpl extends GenericTreeManagerImpl<TeacherCategory, Long>
        implements TeacherCategoryManager {

    TeacherCategoryDao teacherCategoryDao;

    @Autowired
    public void setTeacherCategoryDao(TeacherCategoryDao teacherCategoryDao){
        this.teacherCategoryDao=teacherCategoryDao;
        this.dao=this.teacherCategoryDao;
    }
}
