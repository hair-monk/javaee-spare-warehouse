package edu.zut.cs.javaee.manager.score;

import edu.zut.cs.javaee.manager.base.util.Page;
import edu.zut.cs.javaee.manager.base.util.PageList;
import edu.zut.cs.javaee.manager.score.dao.ScoreDao;
import edu.zut.cs.javaee.manager.score.domain.Score;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;

/**
 * @service make ScoreService become a bean in IOC
 */
@Service
@Transactional
public class ScoreService {

    @Autowired
    private ScoreDao scoreDao;
/**
 * a query statement based on the parameters
 * PageListByHql bysql分页查询
 * @param page
 * @param name
 * @param userId
 * @param role 1 admin	2teacher 3student
 * @return  根据hql查询返回
 * author by wjf
 */
    public PageList<Score> getScoreList(Page page, String name, Integer userId, Integer role) {
        String hql = " from Score where 1=1 ";
        if (!ObjectUtils.isEmpty(name)) {
            hql += " and studentCourse.course.name like '%"+name+"'";
        }
        if (role == 2) {
            //teacher
            hql += " and studentCourse.course.teacher.id = " + userId;
        }

        if (role == 3) {
            hql += " and studentCourse.student.id = " + userId;
        }
        return scoreDao.findPageListByHql(page,hql);
    }
/**
 * 打分  editScore
 * @param score
 * @param id
 * According score set status  
 * auther by wjf
 * scoreDao.get(id) by id acquire entityClass
 */
    public void editScore(BigDecimal score, Integer id) {
        Score scores = scoreDao.get(id);
        scores.setScore(score);
        if (score.intValue() >= 60) {
            scores.getStudentCourse().setStatus(1);
        } else {
            scores.getStudentCourse().setStatus(2);
        }
    }
}
