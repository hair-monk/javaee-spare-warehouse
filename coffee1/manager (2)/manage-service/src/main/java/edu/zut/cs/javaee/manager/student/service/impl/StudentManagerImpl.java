package edu.zut.cs.javaee.manager.student.service.impl;

import edu.zut.cs.javaee.manager.base.service.impl.GenericManagerImpl;
import edu.zut.cs.javaee.manager.student.dao.StudentDao;
import edu.zut.cs.javaee.manager.student.domain.Student;
import edu.zut.cs.javaee.manager.student.service.StudentManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service(value = "StudentManager")
public class StudentManagerImpl extends GenericManagerImpl<Student, Long> implements StudentManager {

//    ProductDao productDao;
//
//    @Autowired
//    public void setProductDao(ProductDao productDao) {
//        this.productDao = productDao;
//        this.dao = this.productDao;
//    }

    StudentDao studentDao;
    @Autowired
    public void setStudentDao(StudentDao studentDao){
        this.studentDao=studentDao;
        this.dao=this.studentDao;
    }
}
