package edu.zut.cs.javaee.manager.studentcourse.service;

import edu.zut.cs.javaee.manager.base.service.GenericTreeManager;
import edu.zut.cs.javaee.manager.student.domain.StudentCategory;
import edu.zut.cs.javaee.manager.studentcourse.domain.StudentCourseType;

public interface StudentCourseTypeManager extends GenericTreeManager<StudentCourseType, Long> {
}
