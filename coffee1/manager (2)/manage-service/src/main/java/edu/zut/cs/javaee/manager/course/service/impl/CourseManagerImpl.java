package edu.zut.cs.javaee.manager.course.service.impl;


import edu.zut.cs.javaee.manager.base.service.impl.GenericManagerImpl;
import edu.zut.cs.javaee.manager.course.dao.CourseDao;
import edu.zut.cs.javaee.manager.course.domain.Course;
import edu.zut.cs.javaee.manager.course.service.CourseManager;
import edu.zut.cs.javaee.manager.student.domain.Student;
import edu.zut.cs.javaee.manager.student.service.StudentManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service(value = "CourseManager")
public class CourseManagerImpl extends GenericManagerImpl<Course, Long> implements CourseManager {
    //    ProductDao productDao;
//
//    @Autowired
//    public void setProductDao(ProductDao productDao) {
//        this.productDao = productDao;
//        this.dao = this.productDao;
//    }



    CourseDao courseDao;

    @Autowired
    public void setCourseDao(CourseDao courseDao)
    {
        this.courseDao = courseDao;
        this.dao = this.courseDao;
    }
}
