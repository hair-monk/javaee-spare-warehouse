package edu.zut.cs.javaee.manager.studentcourse.service.impl;


import edu.zut.cs.javaee.manager.base.service.impl.GenericManagerImpl;
import edu.zut.cs.javaee.manager.student.dao.StudentDao;
import edu.zut.cs.javaee.manager.student.domain.Student;
import edu.zut.cs.javaee.manager.student.service.StudentManager;
import edu.zut.cs.javaee.manager.studentcourse.dao.StudentCourseDao;
import edu.zut.cs.javaee.manager.studentcourse.domain.StudentCourse;
import edu.zut.cs.javaee.manager.studentcourse.service.StudentCourseManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service(value = "StudentCourseManager")
public class StudentCourseManagerImpl extends GenericManagerImpl<StudentCourse, Long> implements StudentCourseManager {

    StudentCourseDao studentCourseDao;
    @Autowired
    public void setStudentCourseDao(StudentCourseDao studentCourseDao){
        this.studentCourseDao = studentCourseDao;
        this.dao = this.studentCourseDao;
    }
}
