package edu.zut.cs.javaee.manager.studentcourse.service;

import edu.zut.cs.javaee.manager.base.service.GenericManagerTestCase;
import edu.zut.cs.javaee.manager.student.domain.Student;
import edu.zut.cs.javaee.manager.student.domain.StudentCategory;
import edu.zut.cs.javaee.manager.student.service.StudentManager;
import edu.zut.cs.javaee.manager.studentcourse.domain.StudentCourse;
import edu.zut.cs.javaee.manager.studentcourse.domain.StudentCourseType;
import org.junit.Test;
import org.junit.Before; 
import org.junit.After;
import org.springframework.beans.factory.annotation.Autowired;

/** 
* StudentCourseService Tester. 
* 
* @author <Authors name> 
* @since <pre>6�� 5, 2021</pre> 
* @version 1.0 
*/ 
public class StudentCourseManagerTest extends GenericManagerTestCase<Long, StudentCourse, StudentCourseManager> {

    public StudentCourseManagerTest() {
        super(StudentCourse.class);
    }

    StudentCourseManager studentCourseManager;

    StudentCourseTypeManager studentCourseTypeManager;

    @Autowired
    public void setStudentCourseManager(StudentCourseManager studentCourseManager){
        this.studentCourseManager=studentCourseManager;
        this.manager=this.studentCourseManager;
    }

    @Autowired
    public void setStudentCourseTypeManager(StudentCourseTypeManager studentCourseTypeManager){
        this.studentCourseTypeManager=studentCourseTypeManager;
    }

    @Test
    public void testBuild() {
        int num_category = 3;
        int num_product = 3;
        for (int i = 0; i < num_category; i++) {
            StudentCourseType category = new StudentCourseType(); // super categories
            category.setName("category_" + i);
            category = this.studentCourseTypeManager.save(category);
            for (int j = 0; j < num_category; j++) {
                StudentCourseType c = new  StudentCourseType(); // current categories
                c.setName("category_" + i + "_" + j);
                c.setParent(category); // set current category's parent category
                c = this.studentCourseTypeManager.save(c);
                for (int k = 0; k < num_product; k++) {
                    StudentCourse student = new StudentCourse(); // product
                    student.setName("name_" + i + "_" + j + "_" + k);
                    student.setStudentCourseType(c); // set product's category
                    student = this.studentCourseManager.save(student);
                    assertNotNull( student);
                }
            }
        }
    }

    private void assertNotNull(StudentCourse student) {
        System.out.println("success");
    }

    @Before
public void before() throws Exception { 
} 

@After
public void after() throws Exception { 
} 

/** 
* 
* Method: getStCourseList(Page page, String name, Integer userId, Integer role, Integer status) 
* 
*/ 
@Test
public void testGetStCourseList() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: daFen(BigDecimal score, Integer id) 
* 
*/ 
@Test
public void testDaFen() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: tuiXuan(Integer id) 
* 
*/ 
@Test
public void testTuiXuan() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: chongXiu(Integer id) 
* 
*/ 
@Test
public void testChongXiu() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: daKa(Integer id) 
* 
*/ 
@Test
public void testDaKa() throws Exception { 
//TODO: Test goes here... 
} 


} 
