package edu.zut.cs.javaee.manager.student.service;

import edu.zut.cs.javaee.manager.base.service.GenericManagerTestCase;
import edu.zut.cs.javaee.manager.score.domain.Score;
import edu.zut.cs.javaee.manager.score.domain.ScoreCategory;
import edu.zut.cs.javaee.manager.score.service.ScoreManager;
import edu.zut.cs.javaee.manager.student.domain.Student;
import edu.zut.cs.javaee.manager.student.domain.StudentCategory;
import org.junit.Test;
import org.junit.Before; 
import org.junit.After;
import org.springframework.beans.factory.annotation.Autowired;

/** 
* StudentService Tester. 
* 
* @author <Authors name> 
* @since <pre>6�� 5, 2021</pre> 
* @version 1.0 
*/ 
public class StudentManagerTest extends GenericManagerTestCase<Long, Student, StudentManager> {


    public StudentManagerTest() {
        super(Student.class);
    }

    StudentManager studentManager;

    StudentCategoryManager studentCategoryManager;

    @Autowired
    public void setStudentManager(StudentManager studentManager){
        this.studentManager=studentManager;
        this.manager=this.studentManager;
    }

    @Autowired
    public void setStudentCategoryManager(StudentCategoryManager studentCategoryManager){
        this.studentCategoryManager=studentCategoryManager;
    }

    @Test
    public void testBuild() {
        int num_category = 3;
        int num_product = 3;
        for (int i = 0; i < num_category; i++) {
            StudentCategory category = new StudentCategory(); // super categories
            category.setName("category_" + i);
            category = this.studentCategoryManager.save(category);
            for (int j = 0; j < num_category; j++) {
                StudentCategory c = new  StudentCategory(); // current categories
                c.setName("category_" + i + "_" + j);
                c.setParent(category); // set current category's parent category
                c = this.studentCategoryManager.save(c);
                for (int k = 0; k < num_product; k++) {
                    Student student = new Student(); // product
                    student.setName("name_" + i + "_" + j + "_" + k);
                    student.setStudentCategory(c); // set product's category
                    student = this.studentManager.save(student);
                    assertNotNull( student);
                }
            }
        }
    }

    private void assertNotNull(Student student) {
        System.out.println("success");
    }
}
