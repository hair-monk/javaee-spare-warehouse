package edu.zut.cs.javaee.manager.kaoqin.service;

import edu.zut.cs.javaee.manager.base.service.GenericTreeManagerTestCase;
import edu.zut.cs.javaee.manager.kaoqin.domain.KaoQinCategory;
import edu.zut.cs.javaee.manager.score.domain.ScoreCategory;
import edu.zut.cs.javaee.manager.score.service.ScoreCategoryManager;
import edu.zut.cs.javaee.manager.score.service.ScoreCategoryManagerTest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class KaoQinCategoryManagerTest extends GenericTreeManagerTestCase<Long, KaoQinCategory,KaoQinCategoryManager> {
    public KaoQinCategoryManagerTest() {
        super(KaoQinCategory.class);
    }
    private static final Logger logger = LogManager.getLogger(KaoQinCategoryManagerTest.class.getName());

    KaoQinCategoryManager KaoQinCategoryManager;

    @Autowired
    public void setGroupManager(KaoQinCategoryManager KaoQinCategoryManager) {
        this.KaoQinCategoryManager = KaoQinCategoryManager;
        this.manager = this.KaoQinCategoryManager;
        Iterable<KaoQinCategory> result = this.KaoQinCategoryManager.findAll();
        if (logger.isInfoEnabled()) {
            logger.info("setGroupManager(GroupManager) - List<Group> result={}", result); //$NON-NLS-1$
        }

    }

}
