package edu.zut.cs.javaee.manager.course.service;

import edu.zut.cs.javaee.manager.base.service.GenericTreeManagerTestCase;
import edu.zut.cs.javaee.manager.course.domain.CourseCategory;
import edu.zut.cs.javaee.manager.kaoqin.domain.KaoQinCategory;
import edu.zut.cs.javaee.manager.kaoqin.service.KaoQinCategoryManager;
import edu.zut.cs.javaee.manager.kaoqin.service.KaoQinCategoryManagerTest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class CourseCategoryManagerTest  extends GenericTreeManagerTestCase<Long, CourseCategory, CourseCategoryManager> {
    public CourseCategoryManagerTest() {
        super(CourseCategory.class);
    }

    private static final Logger logger = LogManager.getLogger(CourseCategoryManagerTest.class.getName());

    CourseCategoryManager courseCategoryManager;

    @Autowired
    public void setGroupManager(CourseCategoryManager courseCategoryManager) {
        this.courseCategoryManager = courseCategoryManager;
        this.manager = this.courseCategoryManager;
        Iterable<CourseCategory> result = this.courseCategoryManager.findAll();
        if (logger.isInfoEnabled()) {
            logger.info("setGroupManager(GroupManager) - List<Group> result={}", result); //$NON-NLS-1$
        }

    }
}
