package edu.zut.cs.javaee.manager.student.service;

import edu.zut.cs.javaee.manager.base.service.GenericTreeManagerTestCase;
import edu.zut.cs.javaee.manager.studentcourse.domain.StudentCourseType;
import edu.zut.cs.javaee.manager.studentcourse.service.StudentCourseTypeManager;
import edu.zut.cs.javaee.manager.studentcourse.service.StudentCourseTypeManagerTest;
import edu.zut.cs.javaee.manager.teacher.domain.TeacherCategory;
import edu.zut.cs.javaee.manager.teacher.service.TeacherCategoryManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class StudentCategoryManagerTest  extends GenericTreeManagerTestCase<Long, TeacherCategory, TeacherCategoryManager> {
    public StudentCategoryManagerTest() {
        super(TeacherCategory.class);
    }

    private static final Logger logger = LogManager.getLogger(StudentCategoryManagerTest.class.getName());

    TeacherCategoryManager teacherCategoryManager;

    @Autowired
    public void setGroupManager(TeacherCategoryManager teacherCategoryManager) {
        this.teacherCategoryManager = teacherCategoryManager;
        this.manager = this.teacherCategoryManager;
        Iterable<TeacherCategory> result = this.teacherCategoryManager.findAll();
        if (logger.isInfoEnabled()) {
            logger.info("setGroupManager(GroupManager) - List<Group> result={}", result); //$NON-NLS-1$
        }

    }
}
