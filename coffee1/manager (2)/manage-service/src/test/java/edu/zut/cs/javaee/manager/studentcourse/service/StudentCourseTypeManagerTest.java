package edu.zut.cs.javaee.manager.studentcourse.service;

import edu.zut.cs.javaee.manager.base.service.GenericTreeManagerTestCase;
import edu.zut.cs.javaee.manager.studentcourse.domain.StudentCourseType;
import edu.zut.cs.javaee.manager.teacher.domain.TeacherCategory;
import edu.zut.cs.javaee.manager.teacher.service.TeacherCategoryManager;
import edu.zut.cs.javaee.manager.teacher.service.TeacherCategoryManagerTest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class StudentCourseTypeManagerTest extends GenericTreeManagerTestCase<Long, StudentCourseType, StudentCourseTypeManager> {
    public StudentCourseTypeManagerTest() {
        super(StudentCourseType.class);
    }

    private static final Logger logger = LogManager.getLogger(StudentCourseTypeManagerTest.class.getName());

    StudentCourseTypeManager studentCourseTypeManager;

    @Autowired
    public void setGroupManager(StudentCourseTypeManager studentCourseTypeManager) {
        this.studentCourseTypeManager = studentCourseTypeManager;
        this.manager = this.studentCourseTypeManager;
        Iterable<StudentCourseType> result = this.studentCourseTypeManager.findAll();
        if (logger.isInfoEnabled()) {
            logger.info("setGroupManager(GroupManager) - List<Group> result={}", result); //$NON-NLS-1$
        }

    }
}
