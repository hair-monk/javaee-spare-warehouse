package edu.zut.cs.javaee.manager.score.service;

import edu.zut.cs.javaee.manager.base.service.GenericTreeManagerTestCase;
import edu.zut.cs.javaee.manager.score.domain.ScoreCategory;
import edu.zut.cs.javaee.manager.student.service.StudentCategoryManagerTest;
import edu.zut.cs.javaee.manager.teacher.domain.TeacherCategory;
import edu.zut.cs.javaee.manager.teacher.service.TeacherCategoryManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class ScoreCategoryManagerTest extends GenericTreeManagerTestCase<Long, ScoreCategory, ScoreCategoryManager> {
    public ScoreCategoryManagerTest() {
        super(ScoreCategory.class);
    }

    private static final Logger logger = LogManager.getLogger(ScoreCategoryManagerTest.class.getName());

    ScoreCategoryManager scoreCategoryManager;

    @Autowired
    public void setGroupManager(ScoreCategoryManager scoreCategoryManager) {
        this.scoreCategoryManager = scoreCategoryManager;
        this.manager = this.scoreCategoryManager;
        Iterable<ScoreCategory> result = this.scoreCategoryManager.findAll();
        if (logger.isInfoEnabled()) {
            logger.info("setGroupManager(GroupManager) - List<Group> result={}", result); //$NON-NLS-1$
        }

    }
}
