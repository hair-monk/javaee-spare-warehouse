package edu.zut.cs.javaee.manager.admin.service;

import edu.zut.cs.javaee.manager.admin.domain.Admin;
import edu.zut.cs.javaee.manager.admin.domain.AdminCategory;
import edu.zut.cs.javaee.manager.base.service.GenericManagerTestCase;
import edu.zut.cs.javaee.manager.course.domain.Course;
import edu.zut.cs.javaee.manager.course.domain.CourseCategory;
import edu.zut.cs.javaee.manager.course.service.CourseManager;
import org.junit.Test;
import org.junit.Before; 
import org.junit.After;
import org.springframework.beans.factory.annotation.Autowired;

/** 
* AdminService Tester. 
* 
* @author <Authors name> 
* @since <pre>6�� 5, 2021</pre> 
* @version 1.0 
*/ 
public class AdminManagerTest extends GenericManagerTestCase<Long, Admin, AdminManager> {

    public AdminManagerTest() {
        super(Admin.class);
    }

    AdminManager adminManager;

    AdminCategoryManager adminCategoryManager;

    @Autowired
    public void setAdminManager(AdminManager adminManager){
        this.adminManager=adminManager;
        this.manager=this.adminManager;
    }

    @Autowired
    public void setAdminCategoryManager(AdminCategoryManager adminCategoryManager){
        this.adminCategoryManager=adminCategoryManager;
    }


    @Test
    public void testBuild() {
        int num_category = 3;
        int num_product = 3;
        for (int i = 0; i < num_category; i++) {
            AdminCategory category = new AdminCategory(); // super categories
            category.setName("category_" + i);
            category = this.adminCategoryManager.save(category);
            for (int j = 0; j < num_category; j++) {
                AdminCategory c = new AdminCategory(); // current categories
                c.setName("category_" + i + "_" + j);
                c.setParent(category); // set current category's parent category
                c = this.adminCategoryManager.save(c);
                for (int k = 0; k < num_product; k++) {
                    Admin admin = new Admin(); // product
                    admin.setName("name_" + i + "_" + j + "_" + k);
                    admin.setAdminCategory(c); // set product's category
                    admin = this.adminManager.save(admin);
                    assertNotNull(admin);
                }
            }
        }
    }

    private void assertNotNull(Admin admin) {
        System.out.println("666");
    }

    @Before
public void before() throws Exception { 
} 

@After
public void after() throws Exception { 
} 

/** 
* 
* Method: getUserInfo(String userName, String passWord) 
* 
*/ 
@Test
public void testGetUserInfo() throws Exception { 
//TODO: Test goes here...

} 


} 
