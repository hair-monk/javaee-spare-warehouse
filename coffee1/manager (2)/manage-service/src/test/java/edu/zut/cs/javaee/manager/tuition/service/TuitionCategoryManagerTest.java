package edu.zut.cs.javaee.manager.tuition.service;

import edu.zut.cs.javaee.manager.base.service.GenericTreeManagerTestCase;
import edu.zut.cs.javaee.manager.tuition.domain.TuitionCategory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class TuitionCategoryManagerTest extends GenericTreeManagerTestCase<Long, TuitionCategory, TuitionCategoryManager> {
    public TuitionCategoryManagerTest() {
        super(TuitionCategory.class);
    }

    private static final Logger logger = LogManager.getLogger(TuitionCategoryManagerTest.class.getName());

    TuitionCategoryManager tuitionCategoryManager;

    @Autowired
    public void setGroupManager( TuitionCategoryManager tuitionCategoryManager) {
        this.tuitionCategoryManager = tuitionCategoryManager;
        this.manager = this.tuitionCategoryManager;
        Iterable<TuitionCategory> result = this.tuitionCategoryManager.findAll();
        if (logger.isInfoEnabled()) {
            logger.info("setGroupManager(GroupManager) - List<Group> result={}", result); //$NON-NLS-1$
        }

    }
}
