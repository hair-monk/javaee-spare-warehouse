package edu.zut.cs.javaee.manager.score.service;

import edu.zut.cs.javaee.manager.base.service.GenericManagerTestCase;
import edu.zut.cs.javaee.manager.kaoqin.domain.KaoQin;
import edu.zut.cs.javaee.manager.kaoqin.domain.KaoQinCategory;
import edu.zut.cs.javaee.manager.kaoqin.service.KaoQinManager;
import edu.zut.cs.javaee.manager.score.domain.Score;
import edu.zut.cs.javaee.manager.score.domain.ScoreCategory;
import org.junit.Test;
import org.junit.Before; 
import org.junit.After;
import org.springframework.beans.factory.annotation.Autowired;

/** 
* ScoreService Tester. 
* 
* @author <Authors name> 
* @since <pre>6�� 5, 2021</pre> 
* @version 1.0 
*/ 
public class ScoreManagerTest extends GenericManagerTestCase<Long, Score, ScoreManager> {

    public ScoreManagerTest() {
        super(Score.class);
    }

    ScoreManager scoreManager;

    ScoreCategoryManager scoreCategoryManager;

    @Autowired
    public void setScoreManager(ScoreManager scoreManager){
        this.scoreManager=scoreManager;
        this.manager=this.scoreManager;
    }

    @Autowired
    public void setScoreCategoryManager( ScoreCategoryManager scoreCategoryManager){
        this.scoreCategoryManager=scoreCategoryManager;
    }
    @Test
    public void testBuild() {
        int num_category = 3;
        int num_product = 3;
        for (int i = 0; i < num_category; i++) {
            ScoreCategory category = new ScoreCategory(); // super categories
            category.setName("category_" + i);
            category = this.scoreCategoryManager.save(category);
            for (int j = 0; j < num_category; j++) {
                ScoreCategory c = new  ScoreCategory(); // current categories
                c.setName("category_" + i + "_" + j);
                c.setParent(category); // set current category's parent category
                c = this.scoreCategoryManager.save(c);
                for (int k = 0; k < num_product; k++) {
                    Score score = new Score(); // product
                    score.setName("name_" + i + "_" + j + "_" + k);
                    score.setScoreCategory(c); // set product's category
                    score = this.scoreManager.save(score);
                    assertNotNull( score);
                }
            }
        }
    }

    private void assertNotNull(Score score) {
        System.out.println("success");
    }

    @Before
public void before() throws Exception { 
} 

@After
public void after() throws Exception { 
} 

/** 
* 
* Method: getScoreList(Page page, String name, Integer userId, Integer role) 
* 
*/ 
@Test
public void testGetScoreList() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: editScore(BigDecimal score, Integer id) 
* 
*/ 
@Test
public void testEditScore() throws Exception { 
//TODO: Test goes here... 
} 


} 
