package edu.zut.cs.javaee.manager.kaoqin.service;

import edu.zut.cs.javaee.manager.base.service.GenericManagerTestCase;
import edu.zut.cs.javaee.manager.course.domain.Course;
import edu.zut.cs.javaee.manager.course.domain.CourseCategory;
import edu.zut.cs.javaee.manager.course.service.CourseManager;
import edu.zut.cs.javaee.manager.kaoqin.domain.KaoQin;
import edu.zut.cs.javaee.manager.kaoqin.domain.KaoQinCategory;
import org.junit.Test;
import org.junit.Before; 
import org.junit.After;
import org.springframework.beans.factory.annotation.Autowired;

/** 
* KaoQinService Tester. 
* 
* @author <Zhao Haobin>
* @since <pre>6�� 5, 2021</pre> 
* @version 1.0 
*/ 
public class KaoQinManagerTest extends GenericManagerTestCase<Long, KaoQin, KaoQinManager> {

    public KaoQinManagerTest() {
        super(KaoQin.class);
    }

    KaoQinManager kaoQinManager;

    KaoQinCategoryManager kaoQinCategoryManager;

    @Autowired
    public void setKaoQinManager(KaoQinManager kaoQinManager){
        this.kaoQinManager=kaoQinManager;
        this.manager=this.kaoQinManager;
    }

    @Autowired
    public void setKaoQinCategoryManager(KaoQinCategoryManager kaoQinCategoryManager){
        this.kaoQinCategoryManager=kaoQinCategoryManager;
    }

    @Test
    public void testBuild() {
        int num_category = 3;
        int num_product = 3;
        for (int i = 0; i < num_category; i++) {
            KaoQinCategory category = new KaoQinCategory(); // super categories
            category.setName("category_" + i);
            category = this.kaoQinCategoryManager.save(category);
            for (int j = 0; j < num_category; j++) {
                KaoQinCategory c = new KaoQinCategory(); // current categories
                c.setName("category_" + i + "_" + j);
                c.setParent(category); // set current category's parent category
                c = this.kaoQinCategoryManager.save(c);
                for (int k = 0; k < num_product; k++) {
                    KaoQin kaoQin = new KaoQin(); // product
                    kaoQin.setName("name_" + i + "_" + j + "_" + k);
                    kaoQin.setKaoQinCategory(c); // set product's category
                    kaoQin = this.kaoQinManager.save(kaoQin);
                    assertNotNull(kaoQin);
                }
            }
        }
    }






    @Before
    public void before() throws Exception {
    }

    @After
    public void after() throws Exception {
    }

    /**
     *
     * Method: getKaoQinList(Page page, String name, Integer userId, Integer role)
     *
     */

    private void assertNotNull(KaoQin kaoQin) {
        System.out.println("666");
    }



@Test
public void testGetKaoQinList() throws Exception { 
//TODO: Test goes here... 
} 


} 
