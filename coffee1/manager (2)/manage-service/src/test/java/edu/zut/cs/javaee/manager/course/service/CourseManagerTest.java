package edu.zut.cs.javaee.manager.course.service;

import edu.zut.cs.javaee.manager.base.dao.BaseDao;
import edu.zut.cs.javaee.manager.base.service.GenericManagerTestCase;
import edu.zut.cs.javaee.manager.course.domain.Course;
import edu.zut.cs.javaee.manager.course.domain.CourseCategory;
import org.junit.Test;
import org.junit.Before; 
import org.junit.After;
import org.springframework.beans.factory.annotation.Autowired;

/** 
* CourseService Tester. 
* 
* @author <Authors name> 
* @since <pre>6�� 5, 2021</pre> 
* @version 1.0 
*/ 
public class CourseManagerTest extends GenericManagerTestCase<Long, Course, CourseManager> {


    public CourseManagerTest() {
        super(Course.class);
    }

    CourseManager courseManager;

    CourseCategoryManager courseCategoryManager;

    @Autowired
    public void setCourseManager(CourseManager courseManager){
        this.courseManager=courseManager;
        this.manager=this.courseManager;
    }

    @Autowired
    public void setCourseCategoryManager(CourseCategoryManager courseCategoryManager){
        this.courseCategoryManager=courseCategoryManager;

    }

    @Test
    public void testBuild() {
        int num_category = 3;
        int num_product = 3;
        for (int i = 0; i < num_category; i++) {
            CourseCategory category = new CourseCategory(); // super categories
            category.setName("category_" + i);
            category = this.courseCategoryManager.save(category);
            for (int j = 0; j < num_category; j++) {
                CourseCategory c = new CourseCategory(); // current categories
                c.setName("category_" + i + "_" + j);
                c.setParent(category); // set current category's parent category
                c = this.courseCategoryManager.save(c);
                for (int k = 0; k < num_product; k++) {
                    Course course = new Course(); // product
                    course.setName("name_" + i + "_" + j + "_" + k);
                    course.setProductCategory(c); // set product's category
                    course = this.courseManager.save(course);
                    assertNotNull(course);
                }
            }
        }
    }

    private void assertNotNull(Course course) {
        System.out.println("366");
    }


    @Before
public void before() throws Exception { 
} 

@After
public void after() throws Exception { 
} 

/** 
* 
* Method: getCourseList(Page page, String name, Integer userId, Integer role) 
* 
*/ 
@Test
public void testGetCourseList() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: add(Course course) 
* 
*/ 
@Test
public void testAdd() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: update(Course c) 
* 
*/ 
@Test
public void testUpdate() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: delete(Course c) 
* 
*/ 
@Test
public void testDelete() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: xuanKe(StudentCourse sc) 
* 
*/ 
@Test
public void testXuanKe() throws Exception { 
//TODO: Test goes here... 
} 


} 
