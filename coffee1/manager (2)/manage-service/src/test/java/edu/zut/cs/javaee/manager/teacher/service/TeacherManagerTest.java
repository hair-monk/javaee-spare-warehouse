package edu.zut.cs.javaee.manager.teacher.service;

import edu.zut.cs.javaee.manager.base.service.GenericManagerTestCase;
import edu.zut.cs.javaee.manager.teacher.domain.Teacher;
import edu.zut.cs.javaee.manager.teacher.domain.TeacherCategory;
import edu.zut.cs.javaee.manager.tuition.domain.Tuition;
import edu.zut.cs.javaee.manager.tuition.domain.TuitionCategory;
import edu.zut.cs.javaee.manager.tuition.service.TuitionManager;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class TeacherManagerTest extends GenericManagerTestCase<Long, Teacher, TeacherManager> {

    TeacherManager teacherManager;
    TeacherCategoryManager teacherCategoryManager;


    public TeacherManagerTest() {
        super(Teacher.class);
    }

    @Autowired
    public void setTeacherManager(TeacherManager teacherManager)
    {
        this.teacherManager = teacherManager;
        this.manager = this.teacherManager;
    }

    @Autowired
    public void setTeacherCategoryManager(TeacherCategoryManager teacherCategoryManager)
    {
        this.teacherCategoryManager = teacherCategoryManager;
    }


    @Test
    public void testBuild() {
        int num_category = 3;
        int num_product = 3;
        for (int i = 0; i < num_category; i++) {
            TeacherCategory category = new TeacherCategory(); // super categories
            category.setName("category_" + i);
            category = this.teacherCategoryManager.save(category);
            for (int j = 0; j < num_category; j++) {
                TeacherCategory c = new TeacherCategory(); // current categories
                c.setName("category_" + i + "_" + j);
                c.setParent(category); // set current category's parent category
                c = this.teacherCategoryManager.save(c);
                for (int k = 0; k < num_product; k++) {
                    Teacher teacher = new Teacher(); // product
                    teacher.setName("name_" + i + "_" + j + "_" + k);
                    teacher.setTeacherCategory(c); // set product's category
                    teacher = this.teacherManager.save(teacher);
                    assertNotNull(teacher);
                }
            }
        }
    }

    private void assertNotNull(Teacher teacher) {
        System.out.println("886");
    }
}
