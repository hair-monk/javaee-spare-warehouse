package edu.zut.cs.javaee.manager.teacher.service;

import edu.zut.cs.javaee.manager.base.service.GenericTreeManagerTestCase;
import edu.zut.cs.javaee.manager.teacher.domain.TeacherCategory;
import edu.zut.cs.javaee.manager.tuition.domain.TuitionCategory;
import edu.zut.cs.javaee.manager.tuition.service.TuitionCategoryManager;
import edu.zut.cs.javaee.manager.tuition.service.TuitionCategoryManagerTest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class TeacherCategoryManagerTest  extends GenericTreeManagerTestCase<Long, TeacherCategory, TeacherCategoryManager> {
    public TeacherCategoryManagerTest() {
        super(TeacherCategory.class);
    }

    private static final Logger logger = LogManager.getLogger(TeacherCategoryManagerTest.class.getName());

    TeacherCategoryManager teacherCategoryManager;

    @Autowired
    public void setGroupManager(TeacherCategoryManager teacherCategoryManager) {
        this.teacherCategoryManager = teacherCategoryManager;
        this.manager = this.teacherCategoryManager;
        Iterable<TeacherCategory> result = this.teacherCategoryManager.findAll();
        if (logger.isInfoEnabled()) {
            logger.info("setGroupManager(GroupManager) - List<Group> result={}", result); //$NON-NLS-1$
        }

    }
}
