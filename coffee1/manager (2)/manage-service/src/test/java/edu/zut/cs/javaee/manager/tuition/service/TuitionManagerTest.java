package edu.zut.cs.javaee.manager.tuition.service;

import edu.zut.cs.javaee.manager.base.service.GenericManagerTestCase;
import edu.zut.cs.javaee.manager.course.domain.Course;
import edu.zut.cs.javaee.manager.course.domain.CourseCategory;
import edu.zut.cs.javaee.manager.tuition.domain.Tuition;
import edu.zut.cs.javaee.manager.tuition.domain.TuitionCategory;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class TuitionManagerTest extends GenericManagerTestCase<Long, Tuition, TuitionManager> {


    TuitionManager tuitionManager;
    TuitionCategoryManager tuitionCategoryManager;


    public TuitionManagerTest() {
        super(Tuition.class);
    }

    @Autowired
    public void setTuitionManager(TuitionManager tuitionManager)
    {
        this.tuitionManager =  tuitionManager;
        this.manager = this.tuitionManager;
    }


    @Autowired
    public  void setTuitionCategoryManager(TuitionCategoryManager tuitionCategoryManager)
    {
        this.tuitionCategoryManager = tuitionCategoryManager;
    }

    @Test
    public void testBuild() {
        int num_category = 3;
        int num_product = 3;
        for (int i = 0; i < num_category; i++) {
            TuitionCategory category = new TuitionCategory(); // super categories
            category.setName("category_" + i);
            category = this.tuitionCategoryManager.save(category);
            for (int j = 0; j < num_category; j++) {
                TuitionCategory c = new TuitionCategory(); // current categories
                c.setName("category_" + i + "_" + j);
                c.setParent(category); // set current category's parent category
                c = this.tuitionCategoryManager.save(c);
                for (int k = 0; k < num_product; k++) {
                    Tuition tuition = new Tuition(); // product
                    tuition.setName("name_" + i + "_" + j + "_" + k);
                    tuition.setTuitionCategory(c); // set product's category
                    tuition = this.tuitionManager.save(tuition);
                    assertNotNull(tuition);
                }
            }
        }
    }

    private void assertNotNull(Tuition tuition) {
        System.out.println("886");
    }

}
