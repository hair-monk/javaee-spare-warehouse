package edu.zut.cs.javaee.manager.admin.service;

import edu.zut.cs.javaee.manager.admin.domain.AdminCategory;
import edu.zut.cs.javaee.manager.base.service.GenericTreeManagerTestCase;
import edu.zut.cs.javaee.manager.course.domain.CourseCategory;
import edu.zut.cs.javaee.manager.course.service.CourseCategoryManager;
import edu.zut.cs.javaee.manager.course.service.CourseCategoryManagerTest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class AdminCategoryManagerTest extends GenericTreeManagerTestCase<Long, AdminCategory, AdminCategoryManager> {
    public AdminCategoryManagerTest() {
        super(AdminCategory.class);
    }

    private static final Logger logger = LogManager.getLogger(AdminCategoryManagerTest.class.getName());

    AdminCategoryManager adminCategoryManager;
    @Autowired
    public void setGroupManager(AdminCategoryManager adminCategoryManager) {
        this.adminCategoryManager = adminCategoryManager;
        this.manager = this.adminCategoryManager;
        Iterable<AdminCategory> result = this.adminCategoryManager.findAll();
        if (logger.isInfoEnabled()) {
            logger.info("setGroupManager(GroupManager) - List<Group> result={}", result); //$NON-NLS-1$
        }

    }
}
