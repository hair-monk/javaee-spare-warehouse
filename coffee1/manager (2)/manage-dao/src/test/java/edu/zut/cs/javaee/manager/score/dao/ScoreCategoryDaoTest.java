package edu.zut.cs.javaee.manager.score.dao;

import edu.zut.cs.javaee.manager.base.dao.GenericTreeDaoTestCase;
import edu.zut.cs.javaee.manager.score.domain.Score;
import edu.zut.cs.javaee.manager.score.domain.ScoreCategory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/** 
* Score Tester. 
* 
* @author <Authors name> 
* @since <pre>6�� 5, 2021</pre> 
* @version 1.0 
*/ 
public class ScoreCategoryDaoTest extends GenericTreeDaoTestCase<Long, ScoreCategory, ScoreCategoryDao> {

    public ScoreCategoryDaoTest(Class<ScoreCategory> persistentClass) {
        super(persistentClass);
    }

    ScoreCategoryDao scoreCategoryDao;

    @Autowired
    public void setScoreCategoryDao(ScoreCategoryDao scoreCategoryDao){
        this.scoreCategoryDao=scoreCategoryDao;
        this.dao=this.scoreCategoryDao;
    }

    @Before
public void before() throws Exception { 
} 

@After
public void after() throws Exception { 
} 

/** 
* 
* Method: getId() 
* 
*/ 
@Test
public void testGetId() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: setId(Integer id) 
* 
*/ 
@Test
public void testSetId() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: getStudentCourse() 
* 
*/ 
@Test
public void testGetStudentCourse() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: setStudentCourse(StudentCourse studentCourse) 
* 
*/ 
@Test
public void testSetStudentCourse() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: getScore() 
* 
*/ 
@Test
public void testGetScore() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: setScore(BigDecimal score) 
* 
*/ 
@Test
public void testSetScore() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: getCreateTime() 
* 
*/ 
@Test
public void testGetCreateTime() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: setCreateTime(Date createTime) 
* 
*/ 
@Test
public void testSetCreateTime() throws Exception { 
//TODO: Test goes here... 
} 


} 
