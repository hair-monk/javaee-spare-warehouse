package edu.zut.cs.javaee.manager.teacher.dao;

import edu.zut.cs.javaee.manager.base.dao.GenericTreeDaoTestCase;
import edu.zut.cs.javaee.manager.studentcourse.dao.StudentCourseTypeDao;
import edu.zut.cs.javaee.manager.studentcourse.domain.StudentCourseType;
import edu.zut.cs.javaee.manager.teacher.domain.TeacherCategory;
import org.junit.Test;
import org.junit.Before; 
import org.junit.After;
import org.springframework.beans.factory.annotation.Autowired;

/** 
* Teacher Tester. 
* 
* @author <Authors name> 
* @since <pre>6�� 5, 2021</pre> 
* @version 1.0 
*/
public class TeacherCategoryDaoTest  extends GenericTreeDaoTestCase<Long, TeacherCategory, TeacherCategoryDao> {


    public TeacherCategoryDaoTest(Class<TeacherCategory> persistentClass) {
        super(persistentClass);
    }

    TeacherCategoryDao teacherCategoryDao;

    @Autowired
    public void setTeacherCategoryDao( TeacherCategoryDao teacherCategoryDao){
        this.teacherCategoryDao=teacherCategoryDao;
        this.dao=this.teacherCategoryDao;

    }
    @Before
public void before() throws Exception { 
} 

@After
public void after() throws Exception { 
} 

/** 
* 
* Method: getId() 
* 
*/ 
@Test
public void testGetId() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: setId(Integer id) 
* 
*/ 
@Test
public void testSetId() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: getUserName() 
* 
*/ 
@Test
public void testGetUserName() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: setUserName(String userName) 
* 
*/ 
@Test
public void testSetUserName() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: getPassword() 
* 
*/ 
@Test
public void testGetPassword() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: setPassword(String password) 
* 
*/ 
@Test
public void testSetPassword() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: getRealName() 
* 
*/ 
@Test
public void testGetRealName() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: setRealName(String realName) 
* 
*/ 
@Test
public void testSetRealName() throws Exception { 
//TODO: Test goes here... 
} 


} 
