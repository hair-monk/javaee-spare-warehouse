package edu.zut.cs.javaee.manager.tuition.dao;

import edu.zut.cs.javaee.manager.base.dao.GenericDaoTestCase;
import edu.zut.cs.javaee.manager.teacher.dao.TeacherDao;
import edu.zut.cs.javaee.manager.teacher.domain.Teacher;
import edu.zut.cs.javaee.manager.tuition.domain.Tuition;
import org.springframework.beans.factory.annotation.Autowired;

public class TuitionDaoTest extends GenericDaoTestCase<Long, Tuition, TuitionDao> {
    public TuitionDaoTest(Class<Tuition> persistentClass) {
        super(persistentClass);
    }
    TuitionDao tuitionDao;

    @Autowired
    public void setTuitionDao(  TuitionDao tuitionDao){
        this.tuitionDao=tuitionDao;
        this.dao=this.tuitionDao;
    }
}
