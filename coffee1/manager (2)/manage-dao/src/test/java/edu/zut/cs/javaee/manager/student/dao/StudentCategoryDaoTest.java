package edu.zut.cs.javaee.manager.student.dao;

import edu.zut.cs.javaee.manager.base.dao.GenericTreeDaoTestCase;
import edu.zut.cs.javaee.manager.student.domain.Student;
import edu.zut.cs.javaee.manager.student.domain.StudentCategory;
import org.junit.Test;
import org.junit.Before; 
import org.junit.After;
import org.springframework.beans.factory.annotation.Autowired;

/** 
* Student Tester. 
* 
* @author <Authors name> 
* @since <pre>6�� 5, 2021</pre> 
* @version 1.0 
*/ 
public class StudentCategoryDaoTest extends GenericTreeDaoTestCase<Long, StudentCategory, StudentCategoryDao> {

    public StudentCategoryDaoTest(Class<StudentCategory> persistentClass) {
        super(persistentClass);
    }

    StudentCategoryDao studentCategoryDao;

    @Autowired
    public void setStudentCategoryDao(StudentCategoryDao studentCategoryDao){
        this.studentCategoryDao=studentCategoryDao;
        this.dao=this.studentCategoryDao;
    }
    @Before
public void before() throws Exception { 
} 

@After
public void after() throws Exception { 
} 

/** 
* 
* Method: getId() 
* 
*/ 
@Test
public void testGetId() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: setId(Integer id) 
* 
*/ 
@Test
public void testSetId() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: getXueHao() 
* 
*/ 
@Test
public void testGetXueHao() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: setXueHao(String xueHao) 
* 
*/ 
@Test
public void testSetXueHao() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: getName() 
* 
*/ 
@Test
public void testGetName() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: setName(String name) 
* 
*/ 
@Test
public void testSetName() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: getSubject() 
* 
*/ 
@Test
public void testGetSubject() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: setSubject(String subject) 
* 
*/ 
@Test
public void testSetSubject() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: getSex() 
* 
*/ 
@Test
public void testGetSex() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: setSex(String sex) 
* 
*/ 
@Test
public void testSetSex() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: getPassword() 
* 
*/ 
@Test
public void testGetPassword() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: setPassword(String password) 
* 
*/ 
@Test
public void testSetPassword() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: getDelFlag() 
* 
*/ 
@Test
public void testGetDelFlag() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: setDelFlag(Integer delFlag) 
* 
*/ 
@Test
public void testSetDelFlag() throws Exception { 
//TODO: Test goes here... 
} 


} 
