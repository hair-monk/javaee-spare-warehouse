package edu.zut.cs.javaee.manager.student.dao;

import edu.zut.cs.javaee.manager.base.dao.GenericDaoTestCase;
import edu.zut.cs.javaee.manager.student.domain.Student;
import org.junit.After;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;

/** 
* StudentDao Tester. 
* 
* @author <Authors name> 
* @since <pre>6�� 5, 2021</pre> 
* @version 1.0 
*/ 
public class StudentDaoTest extends GenericDaoTestCase<Long, Student, StudentDao> {

    public StudentDaoTest(Class<Student> persistentClass) {
        super(persistentClass);
    }
    StudentDao studentDao;

    @Autowired
    public void setStudentDao(StudentDao studentDao){
        this.studentDao=studentDao;
        this.dao=this.studentDao;
    }

    @Before
public void before() throws Exception { 
} 

@After
public void after() throws Exception { 
} 


} 
