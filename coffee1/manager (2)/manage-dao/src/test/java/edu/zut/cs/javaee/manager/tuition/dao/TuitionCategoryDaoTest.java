package edu.zut.cs.javaee.manager.tuition.dao;

import edu.zut.cs.javaee.manager.base.dao.GenericTreeDaoTestCase;
import edu.zut.cs.javaee.manager.teacher.dao.TeacherCategoryDao;
import edu.zut.cs.javaee.manager.teacher.domain.TeacherCategory;
import edu.zut.cs.javaee.manager.tuition.domain.TuitionCategory;
import org.springframework.beans.factory.annotation.Autowired;

public class TuitionCategoryDaoTest extends GenericTreeDaoTestCase<Long, TuitionCategory, TuitionCategoryDao> {
    public TuitionCategoryDaoTest(Class<TuitionCategory> persistentClass) {
        super(persistentClass);
    }

    TuitionCategoryDao tuitionCategoryDao;

    @Autowired
    public void setTuitionCategoryDao(TuitionCategoryDao tuitionCategoryDao){
        this.tuitionCategoryDao=tuitionCategoryDao;
        this.dao=this.tuitionCategoryDao;
    }

}

