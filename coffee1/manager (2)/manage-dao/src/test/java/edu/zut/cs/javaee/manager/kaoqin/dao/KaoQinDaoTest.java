package edu.zut.cs.javaee.manager.kaoqin.dao;

import edu.zut.cs.javaee.manager.base.dao.GenericDaoTestCase;
import edu.zut.cs.javaee.manager.course.dao.CourseDao;
import edu.zut.cs.javaee.manager.course.domain.Course;
import edu.zut.cs.javaee.manager.kaoqin.domain.KaoQin;
import org.junit.Test;
import org.junit.Before; 
import org.junit.After;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.logging.Logger;

/** 
* KaoQinDao Tester. 
* 
* @author <Zhao Haobin>
* @since <pre>6�� 5, 2021</pre> 
* @version 1.0 
*/ 
public class KaoQinDaoTest extends GenericDaoTestCase<Long, KaoQin, KaoQinDao> {


//    CourseDao courseDao;
//
//    public CourseDaoTest() {
//        super(Course.class);
//    }
//
//    @Autowired
//    public void setCourseDao(CourseDao courseDao) {
//        this.courseDao = courseDao;
//        this.dao = this.courseDao;
//    }

    KaoQinDao kaoQinDao;
    public KaoQinDaoTest(){
        super(KaoQin.class);
    }


    @Autowired
    public void setKaoQinDao(KaoQinDao kaoQinDao)
    {
        this.kaoQinDao = kaoQinDao;
        this.dao = this.kaoQinDao;
    }


    @Before
public void before() throws Exception { 
} 

@After
public void after() throws Exception { 
} 



} 
