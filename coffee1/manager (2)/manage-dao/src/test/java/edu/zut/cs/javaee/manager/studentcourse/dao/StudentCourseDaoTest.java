package edu.zut.cs.javaee.manager.studentcourse.dao;

import edu.zut.cs.javaee.manager.base.dao.GenericDaoTestCase;
import edu.zut.cs.javaee.manager.student.dao.StudentDao;
import edu.zut.cs.javaee.manager.student.domain.Student;
import edu.zut.cs.javaee.manager.studentcourse.domain.StudentCourse;
import org.junit.After;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;

/** 
* StudentCourseDao Tester. 
* 
* @author <Authors name> 
* @since <pre>6�� 5, 2021</pre> 
* @version 1.0 
*/ 
public class StudentCourseDaoTest extends GenericDaoTestCase<Long, StudentCourse, StudentCourseDao> {

    public StudentCourseDaoTest(Class<StudentCourse> persistentClass) {
        super(persistentClass);
    }

    StudentCourseDao studentCourseDao;

    @Autowired
    public void setStudentCourseDao(StudentCourseDao studentCourseDao){
        this.studentCourseDao=studentCourseDao;
        this.dao=this.studentCourseDao;
    }
    @Before
public void before() throws Exception { 
} 

@After
public void after() throws Exception { 
} 


} 
