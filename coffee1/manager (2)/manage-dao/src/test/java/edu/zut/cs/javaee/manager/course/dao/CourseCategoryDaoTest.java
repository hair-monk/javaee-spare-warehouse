package edu.zut.cs.javaee.manager.course.dao;

import edu.zut.cs.javaee.manager.base.dao.GenericDaoTestCase;
import edu.zut.cs.javaee.manager.base.dao.GenericTreeDaoTestCase;
import edu.zut.cs.javaee.manager.course.domain.Course;
import edu.zut.cs.javaee.manager.course.domain.CourseCategory;
import org.junit.Test;
import org.junit.Before; 
import org.junit.After;
import org.springframework.beans.factory.annotation.Autowired;

/** 
* Course Tester. 
* 
* @author <Authors name> 
* @since <pre>6�� 5, 2021</pre> 
* @version 1.0 
*/ 
public class CourseCategoryDaoTest extends GenericTreeDaoTestCase<Long, CourseCategory, CourseCategoryDao> {

    CourseCategoryDao courseCategoryDao;

    @Autowired
    public void setCourseCategoryDao(CourseCategoryDao courseCategoryDao)
    {
        this.courseCategoryDao = courseCategoryDao;
        this.dao = this.courseCategoryDao;
    }


    public CourseCategoryDaoTest() {
        super(CourseCategory.class);
    }

    @Before
public void before() throws Exception { 
} 

@After
public void after() throws Exception { 
} 

/** 
* 
* Method: getId() 
* 
*/ 
@Test
public void testGetId() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: setId(Integer id) 
* 
*/ 
@Test
public void testSetId() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: getCourseNo() 
* 
*/ 
@Test
public void testGetCourseNo() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: setCourseNo(String courseNo) 
* 
*/ 
@Test
public void testSetCourseNo() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: getName() 
* 
*/ 
@Test
public void testGetName() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: setName(String name) 
* 
*/ 
@Test
public void testSetName() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: getPeriod() 
* 
*/ 
@Test
public void testGetPeriod() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: setPeriod(String period) 
* 
*/ 
@Test
public void testSetPeriod() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: getScore() 
* 
*/ 
@Test
public void testGetScore() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: setScore(BigDecimal score) 
* 
*/ 
@Test
public void testSetScore() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: getTeacher() 
* 
*/ 
@Test
public void testGetTeacher() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: setTeacher(Teacher teacher) 
* 
*/ 
@Test
public void testSetTeacher() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: getDelFlag() 
* 
*/ 
@Test
public void testGetDelFlag() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: setDelFlag(Integer delFlag) 
* 
*/ 
@Test
public void testSetDelFlag() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: getYear() 
* 
*/ 
@Test
public void testGetYear() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: setYear(String year) 
* 
*/ 
@Test
public void testSetYear() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: getTeacherName() 
* 
*/ 
@Test
public void testGetTeacherName() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: setTeacherName(String teacherName) 
* 
*/ 
@Test
public void testSetTeacherName() throws Exception { 
//TODO: Test goes here... 
} 


} 
