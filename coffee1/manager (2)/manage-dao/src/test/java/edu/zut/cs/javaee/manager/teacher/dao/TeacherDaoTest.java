package edu.zut.cs.javaee.manager.teacher.dao;

import edu.zut.cs.javaee.manager.base.dao.GenericDaoTestCase;
import edu.zut.cs.javaee.manager.teacher.domain.Teacher;
import org.junit.After;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;

/** 
* TeacherDao Tester. 
* 
* @author <Authors name> 
* @since <pre>6�� 5, 2021</pre> 
* @version 1.0 
*/
public class TeacherDaoTest extends GenericDaoTestCase<Long,Teacher,TeacherDao> {
    TeacherDao teacherDao;

    public TeacherDaoTest() {
        super(Teacher.class);
    }

    @Autowired
    public void setTeacherDao(TeacherDao teacherDao) {
        this.teacherDao = teacherDao;
        this.dao = teacherDao;
    }

} 
