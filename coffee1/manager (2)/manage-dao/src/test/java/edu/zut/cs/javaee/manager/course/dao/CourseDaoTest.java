package edu.zut.cs.javaee.manager.course.dao;

import edu.zut.cs.javaee.manager.base.dao.GenericDaoTestCase;
import edu.zut.cs.javaee.manager.course.domain.Course;
import org.junit.Test;
import org.junit.Before; 
import org.junit.After;
import org.springframework.beans.factory.annotation.Autowired;

/** 
* CourseDao Tester. 
* 
* @author <Authors name> 
* @since <pre>6�� 5, 2021</pre> 
* @version 1.0 
*/ 
public class CourseDaoTest extends GenericDaoTestCase<Long, Course, CourseDao> {

    CourseDao courseDao;

    public CourseDaoTest() {
        super(Course.class);
    }

    @Autowired
    public void setCourseDao(CourseDao courseDao) {
        this.courseDao = courseDao;
        this.dao = this.courseDao;
    }

    @Before
public void before() throws Exception { 
} 

@After
public void after() throws Exception { 
} 


} 
