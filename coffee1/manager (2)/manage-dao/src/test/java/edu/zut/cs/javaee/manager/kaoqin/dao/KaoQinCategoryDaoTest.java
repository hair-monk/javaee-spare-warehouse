package edu.zut.cs.javaee.manager.kaoqin.dao;

import edu.zut.cs.javaee.manager.base.dao.GenericTreeDaoTestCase;
import edu.zut.cs.javaee.manager.course.dao.CourseCategoryDao;
import edu.zut.cs.javaee.manager.course.domain.CourseCategory;
import edu.zut.cs.javaee.manager.kaoqin.domain.KaoQinCategory;
import org.junit.Test;
import org.junit.Before; 
import org.junit.After;
import org.springframework.beans.factory.annotation.Autowired;

/** 
* KaoQin Tester. 
* 
* @author <Zhao Haobin>
* @since <pre>6�� 5, 2021</pre> 
* @version 1.0 
*/ 
public class KaoQinCategoryDaoTest extends GenericTreeDaoTestCase<Long, KaoQinCategory, KaoQinCategoryDao> {


    KaoQinCategoryDao kaoQinCategoryDao;

    @Autowired
    public void setKaoQinCategoryDao(KaoQinCategoryDao kaoQinCategoryDao)
    {
        this.kaoQinCategoryDao = kaoQinCategoryDao;
        this.dao = this.kaoQinCategoryDao;
    }

    public KaoQinCategoryDaoTest() {
        super(KaoQinCategory.class);
    }
@Before
public void before() throws Exception { 
} 

@After
public void after() throws Exception { 
} 

/** 
* 
* Method: getId() 
* 
*/ 
@Test
public void testGetId() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: setId(Integer id) 
* 
*/ 
@Test
public void testSetId() throws Exception { 
//TODO: Test goes here...

} 

/** 
* 
* Method: getCourse() 
* 
*/ 
@Test
public void testGetCourse() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: setCourse(StudentCourse course) 
* 
*/ 
@Test
public void testSetCourse() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: getCreateTime() 
* 
*/ 
@Test
public void testGetCreateTime() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: setCreateTime(Date createTime) 
* 
*/ 
@Test
public void testSetCreateTime() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: getTime() 
* 
*/ 
@Test
public void testGetTime() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: setTime(String time) 
* 
*/ 
@Test
public void testSetTime() throws Exception { 
//TODO: Test goes here... 
} 


} 
