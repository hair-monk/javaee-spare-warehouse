package edu.zut.cs.javaee.manager.studentcourse.dao;

import edu.zut.cs.javaee.manager.base.dao.GenericTreeDaoTestCase;
import edu.zut.cs.javaee.manager.studentcourse.domain.StudentCourseType;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/** 
* StudentCourse Tester. 
* 
* @author <Authors name> 
* @since <pre>6�� 5, 2021</pre> 
* @version 1.0 
*/ 
public class StudentCourseTypeDaoTest extends GenericTreeDaoTestCase<Long, StudentCourseType, StudentCourseTypeDao> {

    public StudentCourseTypeDaoTest(Class<StudentCourseType> persistentClass) {
        super(persistentClass);
    }
    StudentCourseTypeDao studentCourseTypeDao;

    @Autowired
    public void setStudentCourseTypeDao(StudentCourseTypeDao studentCourseTypeDao){
        this.studentCourseTypeDao=studentCourseTypeDao;
        this.dao=this.studentCourseTypeDao;

    }


    @Before
public void before() throws Exception { 
} 

@After
public void after() throws Exception { 
} 

/** 
* 
* Method: getId() 
* 
*/ 
@Test
public void testGetId() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: setId(Integer id) 
* 
*/ 
@Test
public void testSetId() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: getStudent() 
* 
*/ 
@Test
public void testGetStudent() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: setStudent(Student student) 
* 
*/ 
@Test
public void testSetStudent() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: getCourse() 
* 
*/ 
@Test
public void testGetCourse() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: setCourse(Course course) 
* 
*/ 
@Test
public void testSetCourse() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: getDelFlag() 
* 
*/ 
@Test
public void testGetDelFlag() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: setDelFlag(Integer delFlag) 
* 
*/ 
@Test
public void testSetDelFlag() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: getStatus() 
* 
*/ 
@Test
public void testGetStatus() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: setStatus(Integer status) 
* 
*/ 
@Test
public void testSetStatus() throws Exception { 
//TODO: Test goes here... 
} 


} 
