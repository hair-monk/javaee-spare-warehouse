package edu.zut.cs.javaee.manager.score.dao;

import edu.zut.cs.javaee.manager.base.dao.GenericDaoTestCase;
import edu.zut.cs.javaee.manager.score.domain.Score;
import org.junit.After;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;

/** 
* ScoreDao Tester. 
* 
* @author <Authors name> 
* @since <pre>6�� 5, 2021</pre> 
* @version 1.0 
*/ 
public class ScoreDaoTest extends GenericDaoTestCase<Long, Score, ScoreDao> {

    public ScoreDaoTest(Class<Score> persistentClass) {
        super(persistentClass);
    }
    @Before
    public void before() throws Exception {
    }

    @After
    public void after() throws Exception {
    }
    ScoreDao scoreDao;

    @Autowired
    public void setScoreDao(ScoreDao scoreDao){
        this.scoreDao=scoreDao;
        this.dao=this.scoreDao;
    }

} 
