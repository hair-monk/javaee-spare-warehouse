package edu.zut.cs.javaee.manager.admin.dao;

import edu.zut.cs.javaee.manager.admin.domain.Admin;
import edu.zut.cs.javaee.manager.base.dao.BaseDao;
import edu.zut.cs.javaee.manager.base.dao.GenericTreeDao;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;

/**
 * 管理员Dao
 * @author Is.f
 *
 */
@Repository
public interface AdminDao extends GenericTreeDao<Admin, Long> {
    List findList(String s, String userName, String passWord);
}
