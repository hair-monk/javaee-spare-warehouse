package edu.zut.cs.javaee.manager.kaoqin.dao;

import edu.zut.cs.javaee.manager.base.dao.BaseDao;
import edu.zut.cs.javaee.manager.base.dao.GenericDao;
import edu.zut.cs.javaee.manager.base.util.Page;
import edu.zut.cs.javaee.manager.base.util.PageList;
import edu.zut.cs.javaee.manager.course.domain.Course;
import edu.zut.cs.javaee.manager.kaoqin.domain.KaoQin;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

/**
 * 
 * @author Zhao Haobin
 *
 */
@Repository
public interface KaoQinDao  extends GenericDao<KaoQin, Long> {

    PageList<KaoQin> findPageListByHql(Page page, String hql);
}
