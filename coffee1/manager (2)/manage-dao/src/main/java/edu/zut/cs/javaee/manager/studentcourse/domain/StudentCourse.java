package edu.zut.cs.javaee.manager.studentcourse.domain;
import edu.zut.cs.javaee.manager.base.domain.BaseEntity;
import edu.zut.cs.javaee.manager.course.domain.Course;
import edu.zut.cs.javaee.manager.student.domain.Student;
import edu.zut.cs.javaee.manager.teacher.domain.Teacher;

import javax.persistence.*;

/**
 * 学生选课的内容
 */
@Entity
@Table(name = "student_course")
public class StudentCourse extends BaseEntity<Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    /**
     * 学生
     */
    @ManyToOne
    @JoinColumn(name = "studentid")
    private Student student = new Student();

    /**
     * 课程
     */
    @ManyToOne
    @JoinColumn(name = "courseid")
    private Course course = new Course();

    /**
     * 0 未删除 1已删除
     */
    @Column(name = "delflag")
    private Integer delFlag;

    /**
     * 0 未考核 1已合格 2不合格 3已退选
     */
    @Column(name = "status")
    private Integer status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Integer getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(Integer delFlag) {
        this.delFlag = delFlag;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public void setName(String s) {
    }

    public void setStudentCourseType(StudentCourseType c) {
    }
}
