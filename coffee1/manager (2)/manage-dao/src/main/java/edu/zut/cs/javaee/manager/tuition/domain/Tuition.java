package edu.zut.cs.javaee.manager.tuition.domain;

import edu.zut.cs.javaee.manager.base.domain.BaseEntity;
import edu.zut.cs.javaee.manager.teacher.domain.Teacher;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "tuition")
public class Tuition  extends BaseEntity<Long> {


    @Column(name = "money")
    private Integer money;

    /**
     * 开课学期
     */
    @Column(name = "year")
    private String year;

    public Integer getMoney() {
        return money;
    }

    public void setMoney(Integer money) {
        this.money = money;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setName(String s) {
    }

    public void setTuitionCategory(TuitionCategory c) {
    }
}
