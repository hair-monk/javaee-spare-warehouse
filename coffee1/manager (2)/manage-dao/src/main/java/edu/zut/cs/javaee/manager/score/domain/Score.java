package edu.zut.cs.javaee.manager.score.domain;
import edu.zut.cs.javaee.manager.base.domain.BaseEntity;
import edu.zut.cs.javaee.manager.student.domain.Student;
import edu.zut.cs.javaee.manager.studentcourse.domain.StudentCourse;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * record score 
 * Entity  "score"
 * auther wjf
 */
@Entity
@Table(name = "score")
public class Score  extends BaseEntity<Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    /**
     * Course record
     *  Introducing foreign keys
     * Associate with the course selection list to obtain the course status
     * 0 未考核 1已合格 2不合格 3已退选
     */
    @OneToOne
    @JoinColumn(name = "studentcourseid")
    private StudentCourse studentCourse = new StudentCourse();

    /**
     * score
     */
    @Column(name = "score")
    private BigDecimal score;

    /**
     * createtime
     */
    @Column(name = "createtime")
    private Date createTime = new Date();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public StudentCourse getStudentCourse() {
        return studentCourse;
    }

    public void setStudentCourse(StudentCourse studentCourse) {
        this.studentCourse = studentCourse;
    }

    public BigDecimal getScore() {
        return score;
    }

    public void setScore(BigDecimal score) {
        this.score = score;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public void setName(String s) {
    }

    public void setScoreCategory(ScoreCategory c) {
    }
}
