package edu.zut.cs.javaee.manager.studentcourse.dao;
import edu.zut.cs.javaee.manager.base.dao.BaseDao;
import edu.zut.cs.javaee.manager.base.dao.GenericDao;
import edu.zut.cs.javaee.manager.base.util.Page;
import edu.zut.cs.javaee.manager.base.util.PageList;
import edu.zut.cs.javaee.manager.student.domain.Student;
import edu.zut.cs.javaee.manager.studentcourse.domain.StudentCourse;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;

/**
 * 管理员dao
 */
@Repository
public interface StudentCourseDao extends GenericDao<StudentCourse, Long> {
    PageList<StudentCourse> findPageListByHql(Page page, String hql);

    StudentCourse get(Integer id);

    List<StudentCourse> findList(String s, Integer id, Integer id1);
}
