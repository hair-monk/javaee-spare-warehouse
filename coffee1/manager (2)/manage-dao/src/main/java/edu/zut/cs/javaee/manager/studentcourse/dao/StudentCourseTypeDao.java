package edu.zut.cs.javaee.manager.studentcourse.dao;

import edu.zut.cs.javaee.manager.base.dao.GenericTreeDao;
import edu.zut.cs.javaee.manager.studentcourse.domain.StudentCourseType;

public interface StudentCourseTypeDao extends GenericTreeDao<StudentCourseType,Long> {
}
