package edu.zut.cs.javaee.manager.tuition.dao;

import edu.zut.cs.javaee.manager.base.dao.GenericDao;

import edu.zut.cs.javaee.manager.tuition.domain.Tuition;
import org.springframework.stereotype.Repository;

@Repository
public interface TuitionDao  extends GenericDao<Tuition, Long> {
}
