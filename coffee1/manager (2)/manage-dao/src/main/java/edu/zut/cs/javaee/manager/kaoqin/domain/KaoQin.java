package edu.zut.cs.javaee.manager.kaoqin.domain;
import edu.zut.cs.javaee.manager.base.domain.BaseEntity;
import edu.zut.cs.javaee.manager.course.domain.Course;
import edu.zut.cs.javaee.manager.student.domain.Student;
import edu.zut.cs.javaee.manager.studentcourse.domain.StudentCourse;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 
 * @author Zhao Haobin
 *this is a table of kaoqin
 */
@Entity
@Table(name = "kaoqin")
public class KaoQin extends BaseEntity<Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    /**
     * 课程
     */
    @ManyToOne
    @JoinColumn(name = "courseid")
    private StudentCourse course = new StudentCourse();

    /**
     * 时间
     */
    @Column(name = "createtime")
    private Date createTime = new Date();

    @Transient
    private String time;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public StudentCourse getCourse() {
        return course;
    }

    public void setCourse(StudentCourse course) {
        this.course = course;
    }

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setName(String s) {
    }

    public void setKaoQinCategory(KaoQinCategory c) {
    }
}
