package edu.zut.cs.javaee.manager.teacher.dao;


import edu.zut.cs.javaee.manager.base.dao.GenericTreeDao;
import edu.zut.cs.javaee.manager.teacher.domain.Teacher;
import edu.zut.cs.javaee.manager.teacher.domain.TeacherCategory;
import edu.zut.cs.javaee.manager.tuition.domain.TuitionCategory;

public interface TeacherCategoryDao  extends GenericTreeDao<TeacherCategory,Long> {
}
