package edu.zut.cs.javaee.manager.student.dao;

import edu.zut.cs.javaee.manager.base.dao.GenericTreeDao;
import edu.zut.cs.javaee.manager.course.domain.CourseCategory;
import edu.zut.cs.javaee.manager.student.domain.StudentCategory;

public interface StudentCategoryDao extends GenericTreeDao<StudentCategory,Long> {
}
