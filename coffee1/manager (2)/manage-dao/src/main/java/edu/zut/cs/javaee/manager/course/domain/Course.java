package edu.zut.cs.javaee.manager.course.domain;
import edu.zut.cs.javaee.manager.base.domain.BaseEntity;
import edu.zut.cs.javaee.manager.teacher.domain.Teacher;

import javax.persistence.*;
import java.math.BigDecimal;
/**
 *
 * @author Is.f
 *
 */
@Entity
@Table(name = "course")
public class Course extends BaseEntity<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    /**
     * 课程号
     */
    @Column(name = "courseno")
    private String courseNo;

    /**
     * 课程名称
     */
    @Column(name = "name")
    private String name;

    /**
     * 学时
     */
    @Column(name = "period")
    private String period;

    /**
     * 学分
     */
    @Column(name = "score")
    private BigDecimal score;

    /**
     * 授课教师
     */
    @ManyToOne
    @JoinColumn(name="teacherid")
    private Teacher teacher = new Teacher();

    /**
     * 0 未删除 1已删除
     */
    @Column(name = "delflag")
    private Integer delFlag;

    /**
     * 开课学期
     */
    @Column(name = "year")
    private String year;

    /**
     * 前置课程
     */
    
    /*未投入使用
    @Column(name = "proCourse")
    private String proCourse;
	*/

    @Transient
    private String teacherName;

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCourseNo() {
        return courseNo;
    }

    public void setCourseNo(String courseNo) {
        this.courseNo = courseNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public BigDecimal getScore() {
        return score;
    }

    public void setScore(BigDecimal score) {
        this.score = score;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public Integer getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(Integer delFlag) {
        this.delFlag = delFlag;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacher.getRealName();
    }

    public void setProductCategory(CourseCategory c) {
    }
}
