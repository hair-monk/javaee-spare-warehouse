package edu.zut.cs.javaee.manager.teacher.domain;
import edu.zut.cs.javaee.manager.base.domain.BaseEntity;

import javax.persistence.*;

/**
 * @author fy
 * teacher pojo
 */
@Entity
@Table(name = "teacher")
public class Teacher extends BaseEntity<Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    /**
     * 用户名
     */
    @Column(name = "username")
    private String userName;

    /**
     * 密码
     */
    @Column(name = "password")
    private String password;

    /**
     * 真实姓名
     */
    @Column(name = "realname")
    private String realName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public void setTeacherCategory(TeacherCategory c) {
    }

    public void setName(String s) {
    }
}
