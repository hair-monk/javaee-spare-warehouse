package edu.zut.cs.javaee.manager.course.dao;

import edu.zut.cs.javaee.manager.base.dao.GenericTreeDao;
import edu.zut.cs.javaee.manager.course.domain.CourseCategory;

public interface CourseCategoryDao extends GenericTreeDao<CourseCategory,Long> {
}
