package edu.zut.cs.javaee.manager.admin.domain;
import edu.zut.cs.javaee.manager.base.domain.BaseTreeEntity;

import javax.persistence.*;
/**
 * 由此生成表
 * @author Is.f
 *
 */
@Entity
@Table(name = "admin")
public class Admin extends BaseTreeEntity<Admin, Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;

    /**
     * 用户名
     */
    @Column(name = "username")
    private String userName;

    /**
     * 密码
     */
    @Column(name = "password")
    private String password;
    public Admin(int id,String username,String password){
        this.id=id;
        this.userName=username;
        this.password=password;
    }

    public Admin() {

    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setName(String s) {
    }


    public void setAdminCategory(AdminCategory c) {
    }
}
