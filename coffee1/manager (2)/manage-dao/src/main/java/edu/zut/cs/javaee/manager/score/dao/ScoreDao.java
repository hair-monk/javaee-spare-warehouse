package edu.zut.cs.javaee.manager.score.dao;

import edu.zut.cs.javaee.manager.base.dao.BaseDao;
import edu.zut.cs.javaee.manager.base.dao.GenericDao;
import edu.zut.cs.javaee.manager.base.util.Page;
import edu.zut.cs.javaee.manager.base.util.PageList;
import edu.zut.cs.javaee.manager.course.domain.Course;
import edu.zut.cs.javaee.manager.score.domain.Score;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

/**
 * Scoredao dao expose service
 * auther by wjf
 */
@Repository
public interface ScoreDao extends GenericDao<Score, Long> {
    Score get(Integer id);

    PageList<Score> findPageListByHql(Page page, String hql);
}
