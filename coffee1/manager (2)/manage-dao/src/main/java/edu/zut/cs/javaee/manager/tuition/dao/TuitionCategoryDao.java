package edu.zut.cs.javaee.manager.tuition.dao;

import edu.zut.cs.javaee.manager.base.dao.GenericTreeDao;
import edu.zut.cs.javaee.manager.course.domain.CourseCategory;
import edu.zut.cs.javaee.manager.tuition.domain.Tuition;
import edu.zut.cs.javaee.manager.tuition.domain.TuitionCategory;

public interface TuitionCategoryDao  extends GenericTreeDao<TuitionCategory,Long> {
}
