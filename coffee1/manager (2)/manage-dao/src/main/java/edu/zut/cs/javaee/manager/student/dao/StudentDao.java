package edu.zut.cs.javaee.manager.student.dao;

import edu.zut.cs.javaee.manager.base.dao.BaseDao;
import edu.zut.cs.javaee.manager.base.dao.GenericDao;
import edu.zut.cs.javaee.manager.base.util.Page;
import edu.zut.cs.javaee.manager.base.util.PageList;
import edu.zut.cs.javaee.manager.course.domain.Course;
import edu.zut.cs.javaee.manager.student.domain.Student;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;

/**
 * 学生dao   
 */
@Repository
public interface StudentDao extends GenericDao<Student, Long> {
    List findList(String s, String userName, String passWord);

    PageList<Student> findPageListByHql(Page page, String hql);

    List<Student> findList(String s, String xueHao);

    Student get(Integer id);

    void update(Student student);
}
