package edu.zut.cs.javaee.manager.score.dao;

import edu.zut.cs.javaee.manager.base.dao.GenericTreeDao;
import edu.zut.cs.javaee.manager.score.domain.ScoreCategory;

public interface ScoreCategoryDao  extends GenericTreeDao<ScoreCategory,Long> {
}
