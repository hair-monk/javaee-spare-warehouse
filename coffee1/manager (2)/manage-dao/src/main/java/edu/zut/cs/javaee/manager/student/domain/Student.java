package edu.zut.cs.javaee.manager.student.domain;
import edu.zut.cs.javaee.manager.base.domain.BaseEntity;

import javax.persistence.*;

/**
 * 学生pojo
 */
@Entity
@Table(name = "student")
public class Student extends BaseEntity<Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    /**
     * 用户名
     */
    @Column(name ="xuehao")
    private String xueHao;

    /**
     * 姓名
     */
    @Column(name ="name")
    private String name;

    /**
     * 专业
     */
    @Column(name ="subject")
    private String subject;

    /**
     * 性别
     */
    @Column(name ="sex")
    private String sex;

    /**
     * 密码
     */
    @Column(name ="password")
    private String password;

    /**
     * 删除标记 1已删除 0 未删除
     */
    @Column(name = "delflag")
    private Integer delFlag;

    public Student(){}


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getXueHao() {
        return xueHao;
    }

    public void setXueHao(String xueHao) {
        this.xueHao = xueHao;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(Integer delFlag) {
        this.delFlag = delFlag;
    }

    public void setStudentCategory(StudentCategory c) {
    }
}
