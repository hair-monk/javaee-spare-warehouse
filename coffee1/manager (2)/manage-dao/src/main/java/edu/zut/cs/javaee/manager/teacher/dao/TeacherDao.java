package edu.zut.cs.javaee.manager.teacher.dao;

import edu.zut.cs.javaee.manager.base.dao.BaseDao;
import edu.zut.cs.javaee.manager.base.dao.GenericDao;
import edu.zut.cs.javaee.manager.teacher.domain.Teacher;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author fy
 *  teacher dao
 */
@Repository
public interface TeacherDao extends GenericDao<Teacher, Long> {
    List<Map<String, Object>> findListBySqlNoEntity(String s);

    List findList(String s, String userName, String passWord);
}
