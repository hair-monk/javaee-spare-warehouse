package edu.zut.cs.javaee.manager.course.dao;

import edu.zut.cs.javaee.manager.base.dao.BaseDao;
import edu.zut.cs.javaee.manager.base.dao.GenericDao;
import edu.zut.cs.javaee.manager.base.util.Page;
import edu.zut.cs.javaee.manager.base.util.PageList;
import edu.zut.cs.javaee.manager.course.domain.Course;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

/**
 * 课程dao
 */
@Repository
public interface CourseDao extends GenericDao<Course, Long> {

    Course get(Integer id);

    void update(Course course);

    PageList<Course> findPageListByHql(Page page, String hql);
}
