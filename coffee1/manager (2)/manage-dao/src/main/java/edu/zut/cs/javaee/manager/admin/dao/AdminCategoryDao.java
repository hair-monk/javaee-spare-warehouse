package edu.zut.cs.javaee.manager.admin.dao;

import edu.zut.cs.javaee.manager.admin.domain.Admin;
import edu.zut.cs.javaee.manager.admin.domain.AdminCategory;
import edu.zut.cs.javaee.manager.base.dao.GenericTreeDao;
import edu.zut.cs.javaee.manager.course.domain.CourseCategory;

public interface AdminCategoryDao  extends GenericTreeDao<AdminCategory,Long> {
}
