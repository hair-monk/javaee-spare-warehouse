package edu.zut.cs.javaee.manager.kaoqin.dao;

import edu.zut.cs.javaee.manager.base.dao.GenericTreeDao;

import edu.zut.cs.javaee.manager.kaoqin.domain.KaoQinCategory;

public interface KaoQinCategoryDao extends GenericTreeDao<KaoQinCategory,Long> {
}
